package fair.axiata.beuniq.com.fair;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Debug;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)

/**
 * Created by beuniq on 25/05/2017.
 */

public class antiGameView extends SurfaceView implements Runnable {

    volatile boolean playing;
    private Thread gameThread = null;

    //These objects will be used for drawing
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;

    private Context thisContext;

    private Float screenDensity;
    private int screenWidth;
    private int screenHeight;

    Typeface plain;
    Typeface thin;

    float gameOverY;

    Bitmap clock;
    Bitmap nextToShow;
    Bitmap iconNextToShow;
    Bitmap iconGoodToShow;
    Bitmap iconBadToShow;
    Bitmap bg;
    Bitmap workerBmp;
    Bitmap waterMachineBmp;
    Bitmap windowBmp;
    Bitmap axiataLogoBmp;
    Bitmap tutorialArrowBmp;
    List<Enemies> enemies;
    List<Bitmap> guy2Frame;
    List<Bitmap> goodItemBmps;
    List<Bitmap> badItemBmps;
    List<Bitmap> goodItemShowBmps;
    List<Bitmap> badItemShowBmps;
    Enemies draggedEnemies;
    Bitmap gameOverBmp;
    Bitmap gameOverWorkerBmp;
    Bitmap continueBmp;

    List<Integer> goodItemPool;
    List<Integer> badItemPool;

    List<numberDisplay> numberDisplayList;

    int goodItemCount;
    int badItemCount;

    long currentTime;
    long elapsedTime;
    float timeMultiplier;
    boolean isGameOver;
    boolean isStart;
    long countdownToStart;
    boolean isMessage;
    float currentTouchX;
    float currentTouchY;
    float previousTouchX;
    float previousTouchY;
    float targetToX;
    float targetToY;
    long highscore;

    long timeToLevel;
    long timeToDismiss;
    String messageToDisplay;

    long frameTime;
    long frameTimeMax;
    int fps;
    int frameNumber;
    int movingTime;
    int timeToSpawn;

    String gameStatus;

    int guyFrame;

    SoundPool soundPool;
    boolean inGame;
    boolean soundOn;

    boolean isTutorial1Show;
    boolean isTutorial2Show;

    boolean hasTutorial1Show;
    boolean hasTutorial2Show;

    float tutorial2X;
    float tutorial2Y;

    String[] goodItemName = {"Bottle","Note","Pendrive", "Apple", "Recycle Bag" , "Movie Ticket", "Pencil", "Keychain", "Coffee", "Ball"};
    String[] badItemName = {"Phone","Cash","Watch", "Camera", "Wallet" , "Diamond", "Loudspeaker", "Holiday Ticket", "Computer", "Hamper"};

    public antiGameView(Context context, Float density) {
        super(context);

        thisContext = context;

        screenDensity = density;

        //initializing drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();

        if (Build.VERSION.SDK_INT >= 11) {
            Point size = new Point();
            try {
                ((Activity) getContext()).getWindowManager().getDefaultDisplay().getRealSize(size);
                screenWidth = size.x;
                screenHeight = size.y;
            } catch (NoSuchMethodError e) {
                screenHeight = display.getHeight();
            }

        } else {
            DisplayMetrics metrics = new DisplayMetrics();
            ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenWidth = display.getWidth();
            screenHeight = display.getHeight();
        }

        soundPool = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        soundPool.load(thisContext, R.raw.bgm2,1);
        soundPool.load(thisContext, R.raw.throwaway,1);
        soundPool.load(thisContext, R.raw.correct,1);

        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
//                soundPool.play(1,0.05f,0.05f,1,-1,1.0f);
//                soundPool.play(2,0.5f,1.0f,1,-1,1.0f);
//                soundPool.play(3,0.1f,0.1f,1,-1,1.0f);
            }
        });

//        init();
    }

    @Override
    public void run()
    {
        while (playing)
        {
            if(inGame)
            {
                if(!soundOn)
                {
                    soundPool.play(1,0.1f,0.1f,1,-1,1.0f);
                    soundOn = true;
                }

                update();
                draw();
            }
        }
    }

    public void init(long highscore)
    {
//        onDestroy();
        //init font
        plain = Typeface.createFromAsset(thisContext.getApplicationContext().getAssets(), "fonts/AllerDisplay.ttf");
        thin = Typeface.createFromAsset(thisContext.getApplicationContext().getAssets(), "fonts/Samuel.ttf");

        guy2Frame = new ArrayList<Bitmap>();

        goodItemBmps = new ArrayList<Bitmap>();
        badItemBmps = new ArrayList<Bitmap>();
        goodItemShowBmps = new ArrayList<Bitmap>();
        badItemShowBmps = new ArrayList<Bitmap>();

        numberDisplayList = new ArrayList<numberDisplay>();

        guy2Frame.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guy_01));
        guy2Frame.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guy_02));

        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_01));
        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_02));
        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_03));
        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_04));
        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_05));
        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_06));
        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_07));
        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_08));
        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_09));
        goodItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_10));

        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_01));
        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_02));
        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_03));
        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_04));
        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_05));
        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_06));
        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_07));
        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_08));
        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_09));
        badItemBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.decline_10));

        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_01));
        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_02));
        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_03));
        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_04));
        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_05));
        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_06));
        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_07));
        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_08));
        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_09));
        goodItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_10));

        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_01));
        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_02));
        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_03));
        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_04));
        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_05));
        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_06));
        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_07));
        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_08));
        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_09));
        badItemShowBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline_10));

        //init all bitmap used
        bg = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.bg_game2);
        clock = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.accept_01);
        clock = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_clock);

        nextToShow = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept_01);
        iconNextToShow = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept);
        iconGoodToShow = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_accept);
        iconBadToShow = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.show_decline);

        workerBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guy_01);
        waterMachineBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.water_machine);
        windowBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.window);
        axiataLogoBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.logo_frame);

        tutorialArrowBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.arrow);

        gameOverBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.gameover_text);
        gameOverWorkerBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.gameover_icon);
        continueBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.btn_continue_01);

        //init enemies list
        enemies = new ArrayList<Enemies>();

        //initialize in game time counter
        currentTime = System.currentTimeMillis();
        elapsedTime = 0;
        timeMultiplier = 1;
        isGameOver = false;
        isStart = false;
        countdownToStart = 0;

        this.highscore = highscore;

        Random rd = new Random();
        timeToSpawn = 1000;//rd.nextInt(2000) + 1000;
        movingTime = 3000;

        timeToLevel = 5000;
        timeToDismiss = 2000;
        isMessage = false;

        fps = 24;
        frameTimeMax = 1000/fps;
        frameTime = 0;

        isTutorial1Show = false;
        isTutorial2Show = false;

        if(this.highscore > 0)
        {
            hasTutorial1Show = true;
            hasTutorial2Show = true;
        }
        else
        {
            hasTutorial1Show = false;
            hasTutorial2Show = false;
        }


        guyFrame = 0;

        goodItemCount = 1;
        badItemCount = 1;
        shuffleItem();
    }

    void shuffleItem()
    {
        goodItemPool = new ArrayList<Integer>();
        badItemPool = new ArrayList<Integer>();

        for(int i = 0; i < 10; i++)
        {
            goodItemPool.add(i);
            badItemPool.add(i);
        }

        for(int i = 0; i < 50; i++)
        {
            for(int j = 0; j < goodItemPool.size() - 1; j++)
            {
                Random rd = new Random();

                if(rd.nextBoolean())
                {
                    int tempInt = goodItemPool.get(j);
                    goodItemPool.set(j, goodItemPool.get(j+1));
                    goodItemPool.set(j + 1, tempInt);
                }
            }
        }

        for(int i = 0; i < 50; i++)
        {
            for(int j = 0; j < badItemPool.size() - 1; j++)
            {
                Random rd = new Random();

                if(rd.nextBoolean())
                {
                    int tempInt = badItemPool.get(j);
                    badItemPool.set(j, badItemPool.get(j+1));
                    badItemPool.set(j + 1, tempInt);
                }
            }
        }
    }

    private void update()
    {
        //update time
        long millis = System.currentTimeMillis();
        long different = (long)((millis - currentTime) * timeMultiplier);
        long differentWithMultiplier = (long)((millis - currentTime) * timeMultiplier);
        if(differentWithMultiplier == 0)
            differentWithMultiplier = 1;

        currentTime = millis;

        if(!isStart)
        {
            if(!isTutorial1Show)
            {
                countdownToStart += different;

                if(!hasTutorial1Show)
                {
                    isTutorial1Show = true;
                    hasTutorial1Show = true;
                }
            }

            if(countdownToStart > 3000)
            {
                isStart = true;
//                countdownToStart = 0;
                gameStatus = "Start";
            }
            else
            {
                gameStatus = String.valueOf(3 - (countdownToStart / 1000));
            }
        }

        //do when game is not over and has started
        if(!isGameOver && isStart)
        {
            if(!isTutorial1Show && !isTutorial2Show)
            {
                elapsedTime += different;
            }

            if(timeToSpawn > 0)
            {
                if(!isTutorial2Show)
                    timeToSpawn -= differentWithMultiplier;
            }
            else
            {
                Random rd = new Random();
                timeToSpawn = 1000;

                int direction = rd.nextInt(4);

                int rdPosX = rd.nextInt(screenWidth);
                int rdPosY = rd.nextInt(screenHeight);

                switch (direction)
                {
                    case 0: //case left
                        rdPosX = 0 - clock.getWidth();
                        break;
                    case 1: //case right
                        rdPosX = screenWidth + clock.getWidth();
                        break;
                    case 2: //case up
                        rdPosY = 0 - clock.getHeight();
                        break;
                    case 3: //case down
                        rdPosY = screenHeight + clock.getHeight();
                        break;
                }

                boolean rdIsGood = rd.nextBoolean();

                Enemies temp = new Enemies(0, rdPosX, rdPosY, rdIsGood, differentWithMultiplier, screenWidth/2, screenHeight/2);

                if(draggedEnemies == null)
                    draggedEnemies = temp;

                enemies.add(temp);
            }

            if(timeToLevel > 0)
            {
                if(!isTutorial2Show)
                    timeToLevel -= differentWithMultiplier;
            }
            else
            {
                if(goodItemCount >= badItemCount)
                {
                    if(badItemCount < badItemPool.size())
                    {
                        badItemCount ++;

                        iconNextToShow = iconBadToShow;
                        nextToShow = badItemBmps.get(badItemPool.get(badItemCount-1));
                        messageToDisplay = badItemName[badItemPool.get(badItemCount-1)];

                        isMessage = true;
                        timeToDismiss = 2000;
                    }
                }
                else
                {
                    if(goodItemCount < goodItemPool.size())
                    {
                        goodItemCount++;

                        iconNextToShow = iconGoodToShow;
                        nextToShow = goodItemBmps.get(goodItemPool.get(goodItemCount-1));
                        messageToDisplay = goodItemName[goodItemPool.get(goodItemCount-1)];

                        isMessage = true;
                        timeToDismiss = 2000;
                    }
                }

                if(timeToSpawn > 500)
                {
                    timeToSpawn -= 100;
                }

                timeToLevel = 5000;

            }

            if(timeToDismiss > 0)
            {
                timeToDismiss -= differentWithMultiplier;
            }
            else
            {
                isMessage = false;
            }
        }

        if(isGameOver)
        {
            if(gameOverY < 150.0f)
                gameOverY += 24.0f;
            else
                gameOverY = 150.0f;
        }

        if(!isTutorial1Show && !isTutorial2Show)
        {
            //move enemies
            for(int i = 0; i < enemies.size(); i++)
            {
                if(!enemies.get(i).isDragged)
                    enemies.get(i).move();

                if(!hasTutorial2Show)
                {
                    if((enemies.get(i).getPosX() > screenWidth/2 && enemies.get(i).getPosX() < screenWidth - (float) Math.ceil(50 * screenDensity))|| (enemies.get(i).getPosX() < screenWidth/2 && enemies.get(i).getPosX() >(float) Math.ceil(50 * screenDensity)))
                    {
                        if((enemies.get(i).getPosY() > screenHeight/2 && enemies.get(i).getPosY() < screenHeight - (float) Math.ceil(50 * screenDensity)) || (enemies.get(i).getPosY() < screenHeight/2 && enemies.get(i).getPosY() > (float) Math.ceil(150 * screenDensity)))
                        {
                            if(!enemies.get(i).isGood)
                            {
                                tutorial2X = enemies.get(i).getPosX();
                                tutorial2Y = enemies.get(i).getPosY();

                                isTutorial2Show = true;
                                hasTutorial2Show = true;

                                break;
                            }
                        }
                    }
                }

                if(enemies.get(i).getPosX() > screenWidth/2 - workerBmp.getWidth()/2 && enemies.get(i).getPosY() > screenHeight/2 && enemies.get(i).getPosX() < screenWidth/2 + workerBmp.getWidth()/2 && enemies.get(i).getPosY() < screenHeight/2 + workerBmp.getHeight()/2)
                {
                    if(!isGameOver)
                    {
                        if(enemies.get(i).isGood)
                        {
                            soundPool.play(3, 1.0f, 1.0f, 1, 0, 1.0f);

                            elapsedTime += 1000;

                            numberDisplay temp = new numberDisplay("+1s",screenWidth / 2,screenHeight / 2);

                            numberDisplayList.add(temp);
                        }
                        else
                        {
                            isGameOver = true;
                            gameOverY = 0.0f;
                        }
                    }
                    enemies.remove(i);
                }
            }
        }

        for(int i = 0; i < numberDisplayList.size(); i++)
        {
            if(numberDisplayList.get(i).posY > screenHeight/2 - (float) Math.ceil(24 * screenDensity))
                numberDisplayList.get(i).moveUp();
            else
                numberDisplayList.remove(i);
        }

        //update frame
        if(frameTime > frameTimeMax)
        {
            if(frameNumber + 1 < fps - 1)
            {
                frameNumber++;
            }
            else
            {
                frameNumber = 0;
            }

            frameTime = 0;

            updateFrame(frameNumber);
        }
        else
        {
            frameTime += different;
        }
    }

    private void draw() {
        //checking if surface is valid
        if (surfaceHolder.getSurface().isValid()) {
            //locking the canvas
            canvas = surfaceHolder.lockCanvas();

            //Set text color
            paint.setColor(Color.argb(255, 76, 76, 76));
            paint.setTextSize((float) Math.ceil(16 * screenDensity));
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTypeface(plain);

            //Draw background
            canvas.drawBitmap(bg, null, new Rect(0,0,screenWidth,screenHeight),paint);

            canvas.drawBitmap(axiataLogoBmp, screenWidth/2 - (float) Math.ceil(80 * screenDensity), screenHeight/2  - (float) Math.ceil(80 * screenDensity), paint);
            canvas.drawBitmap(windowBmp, screenWidth - (4 * windowBmp.getWidth()/5), screenHeight/2 - windowBmp.getHeight()/2 - (float) Math.ceil(40 * screenDensity), paint);
            canvas.drawBitmap(windowBmp, 0 - windowBmp.getWidth()/5, screenHeight/2 - windowBmp.getHeight()/2 - (float) Math.ceil(40 * screenDensity), paint);
            canvas.drawBitmap(waterMachineBmp, screenWidth/2 + (float) Math.ceil(45 * screenDensity), screenHeight/2  - (float) Math.ceil(10 * screenDensity), paint);

//            canvas.drawBitmap(workers.get(frameNumber).getBitmap(), 100, 100, paint);

            if(isMessage)
            {
                //Draw message box
                paint.setColor(Color.argb(255,211,229,245));
                canvas.drawRoundRect(new RectF((int)Math.ceil(10 * screenDensity),screenHeight - (int)Math.ceil(100 * screenDensity) ,screenWidth - (int)Math.ceil(10 * screenDensity),screenHeight - (int)Math.ceil(10 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                paint.setColor(Color.argb(255,255,255,255));
                canvas.drawRoundRect(new RectF((int)Math.ceil(14 * screenDensity),screenHeight - (int)Math.ceil(96 * screenDensity) ,screenWidth - (int)Math.ceil(14 * screenDensity),screenHeight - (int)Math.ceil(18 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                canvas.drawBitmap(nextToShow, screenWidth/2 - nextToShow.getWidth(), screenHeight - (float) Math.ceil(90 * screenDensity), paint);
                canvas.drawBitmap(iconNextToShow, screenWidth/2 - iconNextToShow.getWidth() - nextToShow.getWidth() - (float) Math.ceil(10 * screenDensity), screenHeight - (float) Math.ceil(75 * screenDensity), paint);

                paint.setTypeface(thin);
                paint.setTextSize((float) Math.ceil(25 * screenDensity));
                paint.setTextAlign(Paint.Align.LEFT);
                paint.setColor(Color.argb(255, 58, 123, 178));
                canvas.drawText(messageToDisplay, screenWidth/2 + (float) Math.ceil(20 * screenDensity), screenHeight - (float) Math.ceil(48 * screenDensity), paint);
            }

            if(!isGameOver)
            {
                //Draw Character
                canvas.drawBitmap(guy2Frame.get(guyFrame), screenWidth/2 - workerBmp.getWidth()/2, screenHeight/2 - (float) Math.ceil(40 * screenDensity), paint);

                //Draw Timer
                paint.setTypeface(plain);
                paint.setTextSize((float) Math.ceil(30 * screenDensity));
                paint.setColor(Color.argb(255, 58, 123, 178));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawBitmap(clock,screenWidth/2 - clock.getWidth() - (float) Math.ceil(22 * screenDensity),(float) Math.ceil(15 * screenDensity),paint);
                canvas.drawText(String.format("%02d", elapsedTime/60/1000) + ":" + String.format("%02d", elapsedTime/1000 % 60) , screenWidth/2 + (float) Math.ceil(20 * screenDensity), (float) Math.ceil(38 * screenDensity), paint);

                if(countdownToStart < 3000)
                {
                    paint.setColor(Color.argb(170, 0, 0, 0));
                    canvas.drawRect(0,0,screenWidth,screenHeight,paint);

                    paint.setTypeface(plain);
                    paint.setTextSize((float) Math.ceil(60 * screenDensity));
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText(gameStatus , screenWidth/2, screenHeight/2, paint);

                    if(isTutorial1Show)
                    {
                        paint.setTypeface(thin);
                        paint.setTextSize((float) Math.ceil(25 * screenDensity));
                        paint.setTextAlign(Paint.Align.CENTER);
                        paint.setColor(Color.argb(255, 255, 255, 255));
                        canvas.drawText("Check icon is a good item", screenWidth/2, screenHeight/2 + (float) Math.ceil(60 * screenDensity), paint);
                        canvas.drawText("meanwhile the cross icon is a bad item", screenWidth/2, screenHeight/2 + (float) Math.ceil(90 * screenDensity), paint);

                        canvas.drawBitmap(tutorialArrowBmp, screenWidth/2, screenHeight/2 + (float) Math.ceil(100 * screenDensity), paint);
                    }

                    //Draw message box
                    paint.setColor(Color.argb(255,211,229,245));
                    canvas.drawRoundRect(new RectF((int)Math.ceil(10 * screenDensity),screenHeight - (int)Math.ceil(170 * screenDensity) ,screenWidth - (int)Math.ceil(10 * screenDensity),screenHeight - (int)Math.ceil(10 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                    paint.setColor(Color.argb(255,255,255,255));
                    canvas.drawRoundRect(new RectF((int)Math.ceil(14 * screenDensity),screenHeight - (int)Math.ceil(166 * screenDensity) ,screenWidth - (int)Math.ceil(14 * screenDensity),screenHeight - (int)Math.ceil(18 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                    canvas.drawBitmap(goodItemShowBmps.get(goodItemPool.get(0)), screenWidth/2 - goodItemShowBmps.get(goodItemPool.get(0)).getWidth(), screenHeight - (float) Math.ceil(150 * screenDensity), paint);
                    canvas.drawBitmap(iconGoodToShow, screenWidth/2 - iconGoodToShow.getWidth() - goodItemShowBmps.get(goodItemPool.get(0)).getWidth() - (float) Math.ceil(10 * screenDensity) , screenHeight - (float) Math.ceil(135 * screenDensity), paint);

                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(25 * screenDensity));
                    paint.setTextAlign(Paint.Align.LEFT);
                    paint.setColor(Color.argb(255, 58, 123, 178));
                    canvas.drawText(goodItemName[goodItemPool.get(0)], screenWidth/2 + (float) Math.ceil(20 * screenDensity), screenHeight - (float) Math.ceil(108 * screenDensity), paint);

                    canvas.drawBitmap(badItemShowBmps.get(badItemPool.get(0)), screenWidth/2 - badItemShowBmps.get(badItemPool.get(0)).getWidth(), screenHeight - (float) Math.ceil(90 * screenDensity), paint);
                    canvas.drawBitmap(iconBadToShow, screenWidth/2 - iconBadToShow.getWidth() - badItemShowBmps.get(badItemPool.get(0)).getWidth() - (float) Math.ceil(10 * screenDensity), screenHeight - (float) Math.ceil(75 * screenDensity), paint);

                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(25 * screenDensity));
                    paint.setTextAlign(Paint.Align.LEFT);
                    paint.setColor(Color.argb(255, 58, 123, 178));
                    canvas.drawText(badItemName[badItemPool.get(0)], screenWidth/2 + (float) Math.ceil(20 * screenDensity), screenHeight - (float) Math.ceil(48 * screenDensity), paint);
                }

                for(int i = 0; i < numberDisplayList.size(); i++)
                {
                    paint.setTypeface(plain);
                    paint.setTextSize((float) Math.ceil(30 * screenDensity));
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setColor(Color.argb(255, 76, 76, 76));
                    canvas.drawText(numberDisplayList.get(i).text, numberDisplayList.get(i).posX + 6, numberDisplayList.get(i).posY + 6, paint);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText(numberDisplayList.get(i).text, numberDisplayList.get(i).posX, numberDisplayList.get(i).posY, paint);
                }

                //Draw enemies
                for(int i = 0; i < enemies.size(); i++)
                {
                    canvas.drawBitmap(enemies.get(i).getBitmap(), enemies.get(i).getPosX() - enemies.get(i).getBitmap().getWidth()/2, enemies.get(i).getPosY() - enemies.get(i).getBitmap().getWidth()/2, paint);
                }

                if(isTutorial2Show)
                {
                    paint.setColor(Color.argb(150, 0, 0, 0));
                    canvas.drawRect(0,0,screenWidth,screenHeight,paint);

                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawBitmap(tutorialArrowBmp, tutorial2X, tutorial2Y - tutorialArrowBmp.getHeight(), paint);

                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(25 * screenDensity));
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText("Throw away the unacceptable things", screenWidth/2, screenHeight/2 + (float) Math.ceil(60 * screenDensity), paint);
                }
            }
            else if(isGameOver)
            {
//                canvas.drawText("Tap to restart", screenWidth / 2, screenHeight / 2, paint);

                //Draw message box
                paint.setColor(Color.argb(255,211,229,245));
                canvas.drawRoundRect(new RectF((int)Math.ceil(10 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) ,screenWidth - (int)Math.ceil(10 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(300 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                paint.setColor(Color.argb(255,255,255,255));
                canvas.drawRoundRect(new RectF((int)Math.ceil(14 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(4 * screenDensity) ,screenWidth - (int)Math.ceil(14 * screenDensity), (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(292 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                //Draw gameover Text
                canvas.drawBitmap(gameOverBmp, screenWidth/2 - gameOverBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) - (int)Math.ceil(30 * screenDensity), paint);

                //Draw gameover message
                paint.setTypeface(thin);
                paint.setColor(Color.argb(255, 48, 55, 131));
                paint.setTextSize((float) Math.ceil(25 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("You've violated the gift policy", screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(50 * screenDensity), paint);

                //Draw Highscore
                paint.setTypeface(plain);
                paint.setColor(Color.argb(255, 48, 55, 131));
                paint.setTextSize((float) Math.ceil(26 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                if(elapsedTime/1000 > highscore)
                    canvas.drawText("Highscore : " + elapsedTime/1000, screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(100 * screenDensity), paint);
                else
                    canvas.drawText("Highscore : " + highscore, screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(100 * screenDensity), paint);

                //Draw currentscore
                paint.setTextSize((float) Math.ceil(26 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("Current : " + elapsedTime/1000, screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(135 * screenDensity), paint);

                canvas.drawBitmap(gameOverWorkerBmp, screenWidth/2 - gameOverWorkerBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(170 * screenDensity), paint);

                //Draw continue button
                canvas.drawBitmap(continueBmp, screenWidth/2 - continueBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(320 * screenDensity), paint);
            }
//            else if(!isStart)
//            {
//                canvas.drawText("Tap to start", screenWidth / 2, screenHeight / 2, paint);
//            }

            //Draw debug resolution
//            paint.setTextAlign(Paint.Align.LEFT);
//            paint.setTextSize((float) Math.ceil(10 * screenDensity));
//            canvas.drawText(screenHeight + " " + screenWidth, (float) Math.ceil(5 * screenDensity), (float) Math.ceil(10 * screenDensity), paint);

            //Unlocking the canvas
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int eventAction = event.getAction();

        switch (eventAction) {
            case MotionEvent.ACTION_DOWN:
                for(int i = 0; i < enemies.size(); i++)
                {
                    if(event.getX(0) > enemies.get(i).getPosX() - enemies.get(i).getBitmap().getWidth()/2 && event.getY(0) > enemies.get(i).getPosY() - enemies.get(i).getBitmap().getHeight()/2 && event.getX(0) < enemies.get(i).getPosX() + enemies.get(i).getBitmap().getWidth()/2  && event.getY(0) < enemies.get(i).getPosY() + enemies.get(i).getBitmap().getHeight()/2)
                    {
//                        if(!enemies.get(i).isGood)
//                            enemies.remove(i);
                        enemies.get(i).isDragged = true;
                        draggedEnemies = enemies.get(i);
                    }
                }
                previousTouchX = event.getX(0);
                previousTouchY = event.getY(0);
//                currentTouchX = event.getX(0);
//                currentTouchY = event.getY(0);
                break;
            case MotionEvent.ACTION_MOVE:
//                previousTouchX = currentTouchX;
//                previousTouchY = currentTouchY;

                currentTouchX = event.getX(0);
                currentTouchY = event.getY(0);

                if(!isGameOver && isStart)
                {
                    if(draggedEnemies != null)
                        if(draggedEnemies.isDragged)
                            draggedEnemies.setNewPos(currentTouchX,currentTouchY);
                }
                break;
            case MotionEvent.ACTION_UP:
                if(!isGameOver && isStart)
                {
                    if(!isTutorial1Show && !isTutorial2Show)
                    {
                        if(draggedEnemies != null)
                            if(draggedEnemies.isDragged)
                            {
                                draggedEnemies.isDragged = false;
    //                        draggedEnemies.continueMove();

                                soundPool.play(2, 1.0f, 1.0f, 1, 0, 1.0f);

    //                        if(draggedEnemies.isGood)
    //                        {
    //                            elapsedTime -= 1000;
    //                        }

                                calculateTarget();
                                draggedEnemies.throwAway(targetToX,targetToY);
                            }
                    }
                    else
                    {
                        Log.d("tutorial close","close");
                        isTutorial1Show = false;
                        isTutorial2Show = false;
                    }
                }
                else if(isGameOver)
                {
                    returnScore();
//                    init(highscore);
                }
                else if(!isStart)
                {
                    isTutorial1Show = false;
                    isTutorial2Show = false;
//                    isStart = true;
                }
                break;
        }

        return true;
    }

    public long returnScore()
    {
        return elapsedTime/1000;
    }

    public void calculateTarget()
    {
//        float differentY = previousTouchY - currentTouchY;
//        float differentX = previousTouchX - currentTouchX;
//
//        float m = differentY / differentX;
//
//        float c = previousTouchY - (m * previousTouchX);
//
//        int directionX = 0;
//        int directionY = 0;
//
//        if(differentX <= 0)
//        {
//            directionX = 1;
//        }
//
//        if(differentY <= 0)
//        {
//            directionY = 1;
//        }
//
//        if(directionX == 0)
//        {
//            targetToX = -3000;
//            targetToY = m * previousTouchX + c;
//        }
//        else
//        {
//            targetToX = 3000;
//            targetToY = m * previousTouchX + c;
//        }

        float differentY = currentTouchY - previousTouchY;
        float differentX = currentTouchX - previousTouchX;

        float m = differentY / differentX;

        int directionX = 1;

        if(differentX <= 0)
        {
            directionX = 0;
        }

        if(directionX == 0)
        {
            targetToX = -3000;
            targetToY = (m * (targetToX - previousTouchX)) + previousTouchY;
        }
        else
        {
            targetToX = 3000;
            targetToY = (m * (targetToX - previousTouchX)) + previousTouchY;
        }
    }

    void updateFrame(int frameNumber)
    {
        switch (frameNumber)
        {
            case 0:guyFrame = 1; break;
            case 1:break;
            case 2:break;
            case 3:guyFrame = 0; break;
            case 4:break;
            case 5:break;
            case 6:break;
            case 7:break;
            case 8:break;
            case 9:break;
            case 10:break;
            case 11:break;
            case 12:break;
            case 13:break;
            case 14:break;
            case 15:break;
            case 16:break;
            case 17:break;
            case 18:break;
            case 19:break;
            case 20:break;
            case 21:break;
            case 22:break;
            case 23:break;
        }
    }

    //game pause
    public void pause() {
        playing = false;
        try {
            gameThread.join();
            soundPool.autoPause();
        } catch (InterruptedException e) {
        }
    }

    //game resume
    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
        soundPool.autoResume();
    }

    private class Enemies
    {
        Bitmap bitmap;

        int bmp;

        float posX;
        float posY;

        public boolean isGood;
        public boolean isDragged;

        int stepInvolved;

        float movX;
        float movY;
        float targetX;
        float targetY;

        public Enemies(int bmp, float posX, float posY, boolean isGood, long timeDifferent, float targetX, float targetY)
        {
//            this.bitmap = bitmap;
            Random rd = new Random();

            if(isGood)
            {
                this.bmp = goodItemPool.get(rd.nextInt(goodItemCount));
                this.bitmap = goodItemBmps.get(this.bmp);
            }
            else
            {
                this.bmp = badItemPool.get(rd.nextInt(badItemCount));
                this.bitmap = badItemBmps.get(this.bmp);
            }

            this.posX = posX;
            this.posY = posY;

            this.isGood = isGood;

            stepInvolved = (int) (movingTime/timeDifferent);

            movX = (posX - targetX) / stepInvolved;
            movY = (posY - targetY) / stepInvolved;

            this.targetX = targetX;
            this.targetY = targetY;
        }

        public Bitmap getBitmap()
        {
            return bitmap;
        }

        public float getPosX()
        {
            return posX;
        }

        public float getPosY()
        {
            return posY;
        }

        public void setNewPos(float posX, float posY)
        {
            this.posX = posX;
            this.posY = posY;
        }

        public void move()
        {
            posX -= movX;
            posY -= movY;

            stepInvolved--;
        }

        public void continueMove()
        {
            movX = (posX - targetX) / stepInvolved;
            movY = (posY - targetY) / stepInvolved;
        }

        public void throwAway(float targetX, float targetY)
        {
            movX = (posX - targetX) / stepInvolved;
            movY = (posY - targetY) / stepInvolved;
        }
    }

    private class numberDisplay
    {
        String text;

        float posX;
        float posY;

        public numberDisplay(String text, float posX, float posY)
        {
            this.text = text;

            this.posX = posX;
            this.posY = posY;
        }

        public void moveUp()
        {
            posY -= (float) Math.ceil(2 * screenDensity);
        }
    }

    public void onDestroy()
    {
        if(workerBmp != null)
        {
            workerBmp.recycle();
            workerBmp = null;
        }
        if(bg != null)
        {
            bg.recycle();
            bg = null;
        }
        if(clock != null)
        {
            clock.recycle();
            clock = null;
        }

        if(nextToShow != null)
        {
            nextToShow.recycle();
            nextToShow = null;
        }

        if(iconNextToShow != null)
        {
            iconNextToShow.recycle();
            iconNextToShow = null;
        }
        if(iconGoodToShow != null)
        {
            iconGoodToShow.recycle();
            iconGoodToShow = null;
        }
        if(iconBadToShow != null)
        {
            iconBadToShow.recycle();
            iconBadToShow = null;
        }

        if(guy2Frame != null)
        {
            while (guy2Frame.size() < 0)
            {
                guy2Frame.get(0).recycle();
                guy2Frame.remove(0);
            }
            guy2Frame.clear();
        }
        if(goodItemBmps != null)
        {
            while (goodItemBmps.size() < 0)
            {
                goodItemBmps.get(0).recycle();
                goodItemBmps.remove(0);
            }
            goodItemBmps.clear();
        }
        if(badItemBmps != null)
        {
            while (badItemBmps.size() < 0)
            {
                badItemBmps.get(0).recycle();
                badItemBmps.remove(0);
            }
            badItemBmps.clear();
        }
        if(goodItemShowBmps != null)
        {
            while (goodItemShowBmps.size() < 0)
            {
                goodItemShowBmps.get(0).recycle();
                goodItemShowBmps.remove(0);
            }
            goodItemShowBmps.clear();
        }
        if(badItemShowBmps != null)
        {
            while (badItemShowBmps.size() < 0)
            {
                badItemShowBmps.get(0).recycle();
                badItemShowBmps.remove(0);
            }
            badItemShowBmps.clear();
        }

        if(tutorialArrowBmp != null)
        {
            tutorialArrowBmp.recycle();
            tutorialArrowBmp = null;
        }

        if(gameOverBmp != null)
        {
            gameOverBmp.recycle();
            gameOverBmp = null;
        }
        if(gameOverWorkerBmp != null)
        {
            gameOverWorkerBmp.recycle();
            gameOverWorkerBmp = null;
        }
        if(continueBmp != null)
        {
            continueBmp.recycle();
            continueBmp = null;
        }

        if(waterMachineBmp != null)
        {
            waterMachineBmp.recycle();
            waterMachineBmp = null;
        }

        if(windowBmp != null)
        {
            windowBmp.recycle();
            windowBmp = null;
        }
        if(axiataLogoBmp != null)
        {
            axiataLogoBmp.recycle();
            axiataLogoBmp = null;
        }

        Runtime.getRuntime().gc();
        System.gc();
    }
}
