package fair.axiata.beuniq.com.fair;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    //declare gameview
    private fairGameView gameView;
    private antiGameView agameView;
    private statusGameView sgameView;

    private boolean aBoolean = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        setContentView(R.layout.activity_main);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        final float logicalDensity = metrics.density;

        //init gameview object
        gameView = new fairGameView(this, logicalDensity);
        agameView = new antiGameView(this, logicalDensity);
        sgameView = new statusGameView(this, logicalDensity);

        gameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        agameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        sgameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        //adding gameview to content view
          setContentView(R.layout.activity_main);

        Button buttonGame01 = (Button)findViewById(R.id.game01);
        buttonGame01.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v)
            {
                gameView.init(0);
                setContentView(gameView);
                gameView.soundOn = false;
                gameView.inGame = true;
            }
        });
//
        Button buttonGame02 = (Button)findViewById(R.id.game02);
        buttonGame02.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v)
            {
//                agameView = new antiGameView(MainActivity.this, logicalDensity);
                agameView.init(0);
                setContentView(agameView);
                agameView.soundOn = false;
                agameView.inGame = true;
            }
        });

        Button buttonGame03 = (Button)findViewById(R.id.game03);
        buttonGame03.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v)
            {
                sgameView.init(0);
                setContentView(sgameView);
                sgameView.soundOn = false;
                sgameView.inGame = true;
            }
        });
//        setContentView(gameView);
//        setContentView(agameView);
//        setContentView(sgameView);
        getSupportActionBar().hide();
    }

    //pausing the game when activity is paused
    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
//        if(aBoolean)
        agameView.pause();
        sgameView.pause();
    }

    //running the game when activity is resumed
    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
//        if(aBoolean)
        agameView.resume();
        sgameView.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        gameView.onDestroy();
        agameView.onDestroy();
        sgameView.onDestroy();
    }
}
