package fair.axiata.beuniq.com.fair;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Loader;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Debug;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)

/**
 * Created by beuniq on 16/05/2017.
 */

public class fairGameView extends SurfaceView implements Runnable {

    volatile boolean playing;
    private Thread gameThread = null;

    //These objects will be used for drawing
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;

    private Context thisContext;

    private Float screenDensity;
    private int screenWidth;
    private int screenHeight;

    Bitmap startPageBmp;
    Bitmap workerBmp;
    Bitmap nextFrame;
    Bitmap nextBar;
    Bitmap clock;
    Bitmap bg;
    Bitmap bossBmp;
    Bitmap boxBmp;
    List<Worker> workers;
    List<Bitmap> workerBmps;
    List<Bitmap> worksBmps;
    List<Bitmap> worksPullBmps;
    List<Bitmap> worksSmallBmps;
    List<Bitmap> table2Frame;
    List<Bitmap> tableFainted2Frame;
    List<Bitmap> boss2Frame;
    List<Bitmap> bodyBmps;
    List<Bitmap> bodyFaintedBmps;
    List<Bitmap> faceBmps;
    List<Bitmap> faceRedBmps;
    List<Bitmap> hairBmps;
    List<Bitmap> glassesBmps;
    Works currentWork;
    List<Works> currentWorks;
    Bitmap emptyTable;
    Bitmap gameOverBmp;
    Bitmap gameOverWorkerBmp;
    Bitmap continueBmp;

    Bitmap highlightBmp;
    Bitmap boredBmp;
    Bitmap normalBmp;
    Bitmap tensionBmp;
    Bitmap progressBmp;
    Bitmap boredProgressBmp;
    Bitmap normalProgressBmp;
    Bitmap tensionProgressBmp;
    Bitmap normalExpressionBmp;
    Bitmap boredExpressionBmp;
    Bitmap tensionExpressionBmp;
    Bitmap faintedExpressionBmp;

    Bitmap fingerDownBmp;
    Bitmap fingerUpBmp;
    Bitmap arrowBmp;
    Bitmap fingerDraw;

    Typeface plain;
    Typeface thin;

    float gameOverY;

    enum Emotion {BORED, NORMAL, TENSION};

    long currentTime;
    long elapsedTime;
    long documentTime;
    long documentTimeMax;
    float timeMultiplier;
    boolean isGameOver;
    boolean isStart;
    long countdownToStart;
    long highscore;

    long frameTime;
    long frameTimeMax;
    int fps;
    int frameNumber;

    int bossFrame;

    int timerColor;

//    Axiata - Tan Sri Jamaludin Ibrahim
//    Celcom - Michael Kuehner
//    XL - Dian Siswarini
//    Dialog - Supun Weerasinghe
//    Smart - Thomas Hundt
//    Ncell - Suren J. Amarasekera
//    Robi -Mahtab Uddin Ahmed
//    ADS - Mohd Khairil Abdullah

    String[] indianMaleName = {"Aditya","Ankit","Deepak","Vinay","Pranav","Krishna","Shaurya","Rohan","Shivansh","Abeer","Supun","Suresh Sidhu"};
    String[] indianMaleNameRow2 = {"","","","","","","","","","","Weerasinghe",""};
    String[] indianFemaleName = {"Ishita","Nikita","Aadhya","Kyra","Anaisha","Sarah","Shanaya","Meera","Mishka","Vedhika"};
    String[] indianFemaleNameRow2 = {"","","","","","","","","",""};
    String[] chineseMaleName = {"Wang","Wei Han","Jackson","Andrew","Michael","Jordan","Jack","Harris","Jason","Chou","Michael","Thomas"};
    String[] chineseMaleNameRow2 = {"","","","","","","","","","","Kuehner","Hundt"};
    String[] chineseFemaleName = {"Michelle","Ann","Kim","Zoe","Michelle","Amanda","Chloe","Sharon","Joanne","Nancy"};
    String[] chineseFemaleNameRow2 = {"","","","","","","","","",""};
    String[] malayMaleName = {"Syafiq","Amir","Faizal","Adam","Ariff","Zikri","Umar","Mohamed","Imran","Yusuf","Suren J.","Mahtab","Mohd Khairil","Tan Sri","Mohd Asri"};
    String[] malayMaleNameRow2 = {"","","","","","","","","","","Amarasekera","Uddin Ahmed","Abdullah","Jamaludin Ibrahim","Hassan"};
    String[] malayFemaleName = {"Nurul","Mira","Husna","Nabilah","Sophia","Fara","Nor","Siti","Puteri","Alya","Dian"};
    String[] malayFemaleNameRow2 = {"","","","","","","","","","","Siswarini"};
    String[] foreignMaleName = {"Michael","Jordan","Jack","Harris"};
    String[] foreignFemaleName = {"Michelle","Amanda","Chloe","Sharon"};

    String gameStatus;
    String gameStatusRow2;

    SoundPool soundPool;
    boolean soundOn;
    boolean inGame;

    boolean isTutorial1Show;
    boolean isTutorial2Show;
    boolean isTutorial3Show;

    boolean hasTutorial1Show;
    boolean hasTutorial2Show;
    boolean hasTutorial3Show;

    float tutorialPosx;
    float tutorialPosy;

    public fairGameView(Context context, Float density) {
        super(context);

        thisContext = context;

        screenDensity = density;

        //initializing drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();

        if (Build.VERSION.SDK_INT >= 11) {
            Point size = new Point();
            try {
                ((Activity) getContext()).getWindowManager().getDefaultDisplay().getRealSize(size);
                screenWidth = size.x;
                screenHeight = size.y;
            } catch (NoSuchMethodError e) {
                screenHeight = display.getHeight();
            }

        } else {
            DisplayMetrics metrics = new DisplayMetrics();
            ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenWidth = display.getWidth();
            screenHeight = display.getHeight();
        }

        soundPool = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        soundPool.load(thisContext, R.raw.key01,1);
        soundPool.load(thisContext, R.raw.key02,1);
        soundPool.load(thisContext, R.raw.key03,1);
        soundPool.load(thisContext, R.raw.drop_document,1);
        soundPool.load(thisContext, R.raw.fainted,1);
        soundPool.load(thisContext, R.raw.sad,1);

        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
//                soundPool.play(1,0.01f,0.01f,1,-1,1.0f);
//                soundPool.play(2,0.5f,1.0f,1,-1,1.0f);
//                soundPool.play(3,0.1f,0.1f,1,-1,1.0f);
            }
        });

//        init();
    }

    @Override
    public void run()
    {
        while (playing)
        {
            if(inGame)
            {
                if(!soundOn)
                {
                    soundPool.play(1,0.05f,0.05f,1,-1,1.0f);
                    soundOn = true;
                }

                update();
                draw();
            }
        }
    }

    public void init(long highscore)
    {
//        onDestroy();

        plain = Typeface.createFromAsset(thisContext.getApplicationContext().getAssets(), "fonts/AllerDisplay.ttf");
        thin = Typeface.createFromAsset(thisContext.getApplicationContext().getAssets(), "fonts/Samuel.ttf");

        worksBmps = new ArrayList<Bitmap>();
        worksSmallBmps = new ArrayList<Bitmap>();
        worksPullBmps = new ArrayList<Bitmap>();
        table2Frame = new ArrayList<Bitmap>();
        tableFainted2Frame = new ArrayList<Bitmap>();
        boss2Frame = new ArrayList<Bitmap>();
        bodyBmps = new ArrayList<Bitmap>();
        bodyFaintedBmps = new ArrayList<Bitmap>();
        faceBmps = new ArrayList<Bitmap>();
        faceRedBmps = new ArrayList<Bitmap>();
        hairBmps = new ArrayList<Bitmap>();
        glassesBmps = new ArrayList<Bitmap>();

        //load bitmap asset
        startPageBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.start_page);
        workerBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.worker);
        bg = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.bg);
        nextFrame = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.next_frame);
        nextBar = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.next_bar);
        clock = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_clock);
        bossBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.boss);
        boxBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.box);

        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_00));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_01));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_02));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_03));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_04));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_05));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_06));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_07));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_08));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_09));
        worksBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_10));

        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_01));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_01));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_02));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_03));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_04));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_05));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_06));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_07));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_08));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_09));
        worksPullBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_pull_10));

        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_00));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_01));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_02));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_03));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_04));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_05));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_06));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_07));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_08));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_09));
        worksSmallBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.document_small_10));

        table2Frame.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.table_01));
        table2Frame.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.table_02));

        tableFainted2Frame.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.fainted_state_01));
        tableFainted2Frame.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.fainted_state_02));

        boss2Frame.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.boss_01));
        boss2Frame.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.boss_02));

        bodyBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.body_01));
        bodyBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.body_02));
        bodyBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.body_03));
        bodyBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.body_04));
        bodyBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.body_05));
        bodyBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.body_06));

        bodyFaintedBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.hand_fainted_01));
        bodyFaintedBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.hand_fainted_02));
        bodyFaintedBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.hand_fainted_03));
        bodyFaintedBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.hand_fainted_04));
        bodyFaintedBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.hand_fainted_05));
        bodyFaintedBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.hand_fainted_06));

        faceBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.face_normal_01));
        faceBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.face_normal_02));
        faceBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.face_normal_03));

        faceRedBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.face_red_01));
        faceRedBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.face_red_02));
        faceRedBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.face_red_03));

        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_01_01));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_01_02));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_01_03));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_01_04));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_01_05));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_01_06));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_02_01));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_02_02));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_02_03));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_02_04));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_02_05));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_02_06));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_03_01));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_03_02));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_03_03));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_03_04));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_03_05));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_03_06));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_04_01));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_04_02));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_04_03));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_04_04));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_04_05));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.girlhair_04_06));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_01_01));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_01_02));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_01_03));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_01_04));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_01_05));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_01_06));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_02_01));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_02_02));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_02_03));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_02_04));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_02_05));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_02_06));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_03_01));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_03_02));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_03_03));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_03_04));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_03_05));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_03_06));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_04_01));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_04_02));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_04_03));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_04_04));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_04_05));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_04_06));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_05_01));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_05_02));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_05_03));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_05_04));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_05_05));
        hairBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.guyhair_05_06));

        glassesBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.glasses_01));
        glassesBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.glasses_02));
        glassesBmps.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.glasses_03));

        emptyTable = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.table);

        gameOverBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.gameover_text);
        gameOverWorkerBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.gameover_icon);
        continueBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.btn_continue_01);

        highlightBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.next_bar);

        boredBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.face_01);
        normalBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.face_02);
        tensionBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.face_03);

        progressBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.bar_00);
        boredProgressBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.bar_01);
        normalProgressBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.bar_02);
        tensionProgressBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.bar_03);

        boredExpressionBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.expression_happy);
        normalExpressionBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.expression_sweat);
        tensionExpressionBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.expression_cry);
        faintedExpressionBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.expression_fainted);

        fingerDownBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.finger_down);
        fingerUpBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.finger_up);
        arrowBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.arrow);

        //initialize workers
        workers = new ArrayList<Worker>();

        for(int i = 0; i < 6; i++) {

            Worker temp = new Worker(workerBmp,
                    screenWidth/2 - workerBmp.getWidth()/2 - workerBmp.getWidth() - (float) Math.ceil((60) * screenDensity) + ((i%3) * workerBmp.getWidth()) + ((i%3) * (float) Math.ceil((60) * screenDensity)),
                    screenHeight - (workerBmp.getHeight() * (i/3 + 2) ) - (float) Math.ceil(((100 * (i/3)) - 25) * screenDensity),
                    workerBmp.getWidth(),
                    workerBmp.getHeight());

            workers.add(temp);
        }

        //initialize current works and work stacks
        currentWorks = new ArrayList<Works>();

        for(int i = 0; i < 7; i++)
        {
            Random rd = new Random();
            int documentCount = rd.nextInt(4) + 2;

            Works temp = new Works(worksBmps.get(documentCount + 1),documentCount + 1);

            currentWorks.add(temp);
        }

        currentWork = currentWorks.get(0);

        //initialize in game time counter
        currentTime = System.currentTimeMillis();
        elapsedTime = 0;
        documentTimeMax = 5000;
        documentTime = 0;
        countdownToStart = 0;
        timeMultiplier = 1;
        isGameOver = false;
        isStart = false;
        this.highscore = highscore;

        isTutorial1Show = false;
        isTutorial2Show = false;
        isTutorial3Show = false;

        if(this.highscore > 0)
        {
            hasTutorial1Show = true;
            hasTutorial2Show = true;
            hasTutorial3Show = true;
        }
        else
        {
            hasTutorial1Show = false;
            hasTutorial2Show = false;
            hasTutorial3Show = false;
        }

        gameStatus = "Game Start";
        gameStatusRow2 = "";

        fingerDraw = fingerDownBmp;

        fps = 24;
        frameTimeMax = 1000/fps;
        frameTime = 0;

        bossFrame = 0;

        timerColor = Color.argb(255,78,255,160);
    }

    private void update()
    {
        //update time
        long millis = System.currentTimeMillis();
        long different = (long)((millis - currentTime));
        long differentWithMultiplier = (long)((millis - currentTime) * timeMultiplier);
        currentTime = millis;

        if(!isStart)
        {
            countdownToStart += different;

            if(countdownToStart > 3000)
            {
                isStart = true;
//                countdownToStart = 0;
                gameStatus = "Start";
                gameStatusRow2 = "";

                if(!hasTutorial1Show)
                {
                    isTutorial1Show = true;
                    hasTutorial1Show = true;
                }
            }
            else
            {
                gameStatus = String.valueOf(3 - (countdownToStart / 1000));
                gameStatusRow2 = "";
            }
        }

        if(isTutorial1Show)
        {
            if(tutorialPosy < (float) Math.ceil((160) * screenDensity))
            {
                tutorialPosy += (float) Math.ceil((7) * screenDensity);
                if(tutorialPosy > (float) Math.ceil((140) * screenDensity))
                {
                    fingerDraw = fingerUpBmp;
                }
            }
            else
            {
                tutorialPosy = 0;
                fingerDraw = fingerDownBmp;
            }
        }

        //do when game is not over and has started
        if(!isGameOver && isStart)
        {
            if(!isTutorial1Show && !isTutorial2Show && !isTutorial3Show)
            {
                elapsedTime += different;
            }

            if(documentTime + differentWithMultiplier <= 5000)
            {
                if(!isTutorial1Show && !isTutorial2Show && !isTutorial3Show)
                {
                    documentTime += differentWithMultiplier;
                }

                if(documentTime > 3000)
                {
                    gameStatus = "Hurry Up";
                    gameStatusRow2 = "";
                }

                if(documentTime >= 0 && documentTime < 2000)
                {
                    timerColor = Color.argb(255,78,255,160);
                }
                else if(documentTime >= 2000 && documentTime < 4000)
                {
                    timerColor = Color.argb(255,255,191,0);
                }
                else if(documentTime >= 4000)
                {
                    timerColor = Color.argb(255,255,31,0);
                }
            }
            else
            {
                documentTime = 5000; // when you die
                isGameOver = true;
                gameOverY = 0.0f;
            }

            //Workers doing work
            if(!isTutorial1Show && !isTutorial2Show && !isTutorial3Show)
            {
                for(int i = 0; i < workers.size(); i++)
                {
                    workers.get(i).doingWork(differentWithMultiplier);
                }
            }

            //increase difficulties every minutes
            timeMultiplier = 1 + (0.1f * elapsedTime/5/1000);
        }

        if(isGameOver)
        {
            if(gameOverY < 150.0f)
                gameOverY += 24.0f;
            else
                gameOverY = 150.0f;
        }

        //update frame
        if(frameTime > frameTimeMax)
        {
            if(frameNumber + 1 < fps - 1)
            {
                frameNumber++;
            }
            else
            {
                frameNumber = 0;
            }

            frameTime = 0;

            updateFrame(frameNumber);
            for(int i = 0; i < workers.size(); i++)
            {
                workers.get(i).updateFrame(frameNumber);
            }
        }
        else
        {
            frameTime += different;
        }
    }

    private void draw() {
        //checking if surface is valid
        if (surfaceHolder.getSurface().isValid()) {
            //locking the canvas
            canvas = surfaceHolder.lockCanvas();

            //Set text color
            paint.setColor(Color.argb(255, 76, 76, 76));
            paint.setTextSize((float) Math.ceil(16 * screenDensity));
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTypeface(plain);

            //Draw background
            canvas.drawBitmap(bg, null, new Rect(0,0,screenWidth,screenHeight),paint);
//            canvas.drawBitmap(highlightBmp, null, new Rect(0,0,screenWidth,screenHeight),paint);

            if(!isGameOver)
            {
                //Draw boss
                canvas.drawBitmap(boss2Frame.get(bossFrame), (int)Math.ceil(30 * screenDensity), (int)Math.ceil(150 * screenDensity), paint);

                //Draw document frame
//                paint.setColor(Color.argb(255,78,255,160));
                paint.setColor(timerColor);
                canvas.drawRoundRect(new RectF(screenWidth/2 - (int)Math.ceil(21 * screenDensity),(int)Math.ceil(250 * screenDensity) - (int)Math.ceil(85 * screenDensity * documentTime/documentTimeMax) ,screenWidth/2 +(int)Math.ceil(21 * screenDensity),(int)Math.ceil(250 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);
//                canvas.drawBitmap(nextBar, null, new Rect(screenWidth/2 - nextFrame.getWidth()/2,(int)Math.ceil(155 * screenDensity) - (int)Math.ceil(85 * screenDensity * documentTime/documentTimeMax) ,screenWidth/2 - nextFrame.getWidth()/2 + (int)Math.ceil(44 * screenDensity),(int)Math.ceil(155 * screenDensity)),paint);
                canvas.drawBitmap(nextFrame, screenWidth/2 - nextFrame.getWidth()/2,(float) Math.ceil(150 * screenDensity),paint);

                //Drawing the employee
                for(int i = 0; i < workers.size(); i++)
                {
                    if(!workers.get(i).isDie)
                    {
                        if(workers.get(i).isHighlighted)
                        {
                            //181 230 255
                            paint.setColor(Color.argb(255,101,115,173));
                            canvas.drawRoundRect(new RectF((workers.get(i).getX() - (float) Math.ceil(25 * screenDensity)), (workers.get(i).getY() - (float) Math.ceil(50 * screenDensity)), (workers.get(i).getX() + (float) Math.ceil(25 * screenDensity) + workerBmp.getWidth()),(workers.get(i).getY() + (float) Math.ceil(10 * screenDensity) + workerBmp.getHeight())),(float)Math.ceil(10 * screenDensity),(float)Math.ceil(10 * screenDensity),paint);
                        }

                        //Draw workers current document image
                        canvas.drawBitmap(worksSmallBmps.get(workers.get(i).getDocumentCount()), workers.get(i).getX() + workerBmp.getWidth() - worksSmallBmps.get(workers.get(i).getDocumentCount()).getWidth(), workers.get(i).getY() + (float) Math.ceil(8 * screenDensity), paint);
                        //Draw workers avatar image
//                        canvas.drawBitmap(workers.get(i).getBitmap(), workers.get(i).getX(), workers.get(i).getY(), paint);
                        canvas.drawBitmap(emptyTable, workers.get(i).getX(), workers.get(i).getY(), paint);
                        //Draw workers body
                        canvas.drawBitmap(bodyBmps.get(workers.get(i).body), workers.get(i).getX(), workers.get(i).getY(), paint);

                        //Drawing emotion and progress bar
                        switch (workers.get(i).emotion)
                        {
                            case BORED:
                                canvas.drawBitmap(boredBmp,workers.get(i).getX() + workers.get(i).getWidth()/2 - boredBmp.getWidth()/2, workers.get(i).getY() - boredBmp.getHeight() - (float) Math.ceil(20 * screenDensity),paint);
                                canvas.drawBitmap(boredProgressBmp,workers.get(i).getX() + workers.get(i).getWidth()/2 - progressBmp.getWidth()/2, workers.get(i).getY() - progressBmp.getHeight() - (float) Math.ceil(12 * screenDensity),paint);
                                canvas.drawBitmap(faceBmps.get(workers.get(i).face), workers.get(i).getX(), workers.get(i).getY(), paint);
                                canvas.drawBitmap(boredExpressionBmp, workers.get(i).getX(), workers.get(i).getY(), paint);
                                paint.setColor(Color.argb(255, 0, 255, 214));
                                break;
                            case NORMAL:
                                canvas.drawBitmap(normalBmp,workers.get(i).getX() + workers.get(i).getWidth()/2 - normalBmp.getWidth()/2, workers.get(i).getY() - normalBmp.getHeight() - (float) Math.ceil(20 * screenDensity),paint);
                                canvas.drawBitmap(normalProgressBmp,workers.get(i).getX() + workers.get(i).getWidth()/2 - progressBmp.getWidth()/2, workers.get(i).getY() - progressBmp.getHeight() - (float) Math.ceil(12 * screenDensity),paint);
                                canvas.drawBitmap(faceBmps.get(workers.get(i).face), workers.get(i).getX(), workers.get(i).getY(), paint);
                                canvas.drawBitmap(normalExpressionBmp, workers.get(i).getX(), workers.get(i).getY(), paint);
                                paint.setColor(Color.argb(255, 255, 240, 70));
                                break;
                            case TENSION:
                                canvas.drawBitmap(tensionBmp,workers.get(i).getX() + workers.get(i).getWidth()/2 - tensionBmp.getWidth()/2, workers.get(i).getY() - tensionBmp.getHeight() - (float) Math.ceil(20 * screenDensity),paint);
                                canvas.drawBitmap(tensionProgressBmp,workers.get(i).getX() + workers.get(i).getWidth()/2 - progressBmp.getWidth()/2, workers.get(i).getY() - progressBmp.getHeight() - (float) Math.ceil(12 * screenDensity),paint);
                                canvas.drawBitmap(faceRedBmps.get(workers.get(i).face), workers.get(i).getX(), workers.get(i).getY(), paint);
                                canvas.drawBitmap(tensionExpressionBmp, workers.get(i).getX(), workers.get(i).getY(), paint);
                                paint.setColor(Color.argb(255, 255, 79, 83));
                                break;
                        }

                        //Draw hair
                        canvas.drawBitmap(hairBmps.get(workers.get(i).hair), workers.get(i).getX(), workers.get(i).getY(), paint);

                        //Draw glasses if have
                        if(workers.get(i).hasGlass == 1)
                            canvas.drawBitmap(glassesBmps.get(workers.get(i).glasses), workers.get(i).getX(), workers.get(i).getY(), paint);

                        //Draw computer
                        canvas.drawBitmap(table2Frame.get(workers.get(i).tableFrame), workers.get(i).getX(), workers.get(i).getY(), paint);

                        //Draw progress bar foreground
                        canvas.drawBitmap(progressBmp, null, new Rect((int)(workers.get(i).getX() + workers.get(i).getWidth()/2 - progressBmp.getWidth()/2),(int)(workers.get(i).getY() - progressBmp.getHeight() - (float) Math.ceil(12 * screenDensity)),(int)(workers.get(i).getX() + workers.get(i).getWidth()/2 - progressBmp.getWidth()/2 + (progressBmp.getWidth() * (1 - workers.get(i).getWorkingTimePercentage()))),(int)(workers.get(i).getY() - progressBmp.getHeight() - (float) Math.ceil(12 * screenDensity) + progressBmp.getHeight())),paint);

                        //Draw workers current document number
                        paint.setTypeface(plain);
                        canvas.drawText(workers.get(i).getDocumentCount()+"",workers.get(i).getX() + workerBmp.getWidth() - worksSmallBmps.get(workers.get(i).getDocumentCount()).getWidth()/2,workers.get(i).getY() + (float) Math.ceil(5 * screenDensity),paint);
                    }
                    else
                    {
                        //Draw fainted character
                        canvas.drawBitmap(emptyTable, workers.get(i).getX(), workers.get(i).getY(), paint);
                        canvas.drawBitmap(bodyFaintedBmps.get(workers.get(i).body), workers.get(i).getX(), workers.get(i).getY(), paint);
                        canvas.drawBitmap(faceBmps.get(workers.get(i).face), workers.get(i).getX() + (float) Math.ceil(10 * screenDensity), workers.get(i).getY() + (float) Math.ceil(17 * screenDensity), paint);
                        canvas.drawBitmap(hairBmps.get(workers.get(i).hair), workers.get(i).getX() + (float) Math.ceil(10 * screenDensity), workers.get(i).getY() + (float) Math.ceil(17 * screenDensity), paint);
                        canvas.drawBitmap(faintedExpressionBmp, workers.get(i).getX() + (float) Math.ceil(11 * screenDensity), workers.get(i).getY() + (float) Math.ceil(17 * screenDensity), paint);
                        if(workers.get(i).hasGlass == 1)
                            canvas.drawBitmap(glassesBmps.get(workers.get(i).glasses), workers.get(i).getX() + (float) Math.ceil(10 * screenDensity), workers.get(i).getY() + (float) Math.ceil(17 * screenDensity), paint);
                        canvas.drawBitmap(tableFainted2Frame.get(workers.get(i).tableFrame), workers.get(i).getX() - (float) Math.ceil(10 * screenDensity) , workers.get(i).getY(), paint);
                    }

                    //Draw workers name
                    paint.setTypeface(thin);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText(workers.get(i).name,workers.get(i).getX() + workerBmp.getWidth() / 2 ,workers.get(i).getY() + workerBmp.getHeight() + (float) Math.ceil(20 * screenDensity) ,paint);
                    canvas.drawText(workers.get(i).nameRow2,workers.get(i).getX() + workerBmp.getWidth() / 2 ,workers.get(i).getY() + workerBmp.getHeight() + (float) Math.ceil(38 * screenDensity) ,paint);
                }

                //Draw current job
                canvas.drawBitmap(currentWork.getBitmap(), currentWork.getPosX(), currentWork.getPosY(), paint );
                paint.setColor(Color.argb(255, 255, 255, 255));
                paint.setTextSize((float) Math.ceil(14 * screenDensity));
                paint.setTypeface(plain);
                canvas.drawText(currentWork.getDocumentCount()+"",screenWidth/2, (float) Math.ceil(163 * screenDensity), paint);

                //Draw current job queue
                for(int i = 1; i < currentWorks.size(); i++)
                {
                    canvas.drawBitmap(currentWorks.get(i).getBitmap(),screenWidth/2 + (currentWorks.get(i).getBitmap().getWidth() * i) + (currentWorks.get(i).getBitmap().getWidth() / 4 * (i - 1)), (float) Math.ceil(170 * screenDensity), paint );
                    paint.setColor(Color.argb(255, 76, 76, 76));
                    canvas.drawText(currentWorks.get(i).getDocumentCount()+"",screenWidth/2 + (currentWorks.get(i).getBitmap().getWidth() * i) + currentWorks.get(i).getBitmap().getWidth() / 2 + (currentWorks.get(i).getBitmap().getWidth() / 4 * (i - 1)), (float) Math.ceil(164 * screenDensity), paint);
                }

                //Draw Timer
                paint.setTypeface(plain);
                paint.setTextSize((float) Math.ceil(30 * screenDensity));
                paint.setColor(Color.argb(255, 58, 123, 178));
                canvas.drawBitmap(clock,screenWidth/2 - clock.getWidth() - (float) Math.ceil(22 * screenDensity),(float) Math.ceil(15 * screenDensity),paint);
                canvas.drawText(String.format("%02d", elapsedTime/60/1000) + ":" + String.format("%02d", elapsedTime/1000 % 60) , screenWidth/2 + (float) Math.ceil(20 * screenDensity), (float) Math.ceil(38 * screenDensity), paint);

                if(isTutorial2Show || isTutorial3Show)
                {
                    paint.setColor(Color.argb(150, 0, 0, 0));
                    canvas.drawRect(0,0,screenWidth,screenHeight,paint);

                    paint.setColor(Color.argb(255, 0, 0, 0));
                }

                //Draw message box
                paint.setColor(Color.argb(255,211,229,245));
                canvas.drawRoundRect(new RectF((int)Math.ceil(10 * screenDensity),(int)Math.ceil(50 * screenDensity) ,screenWidth - (int)Math.ceil(10 * screenDensity),(int)Math.ceil(130 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                paint.setColor(Color.argb(255,255,255,255));
                canvas.drawRoundRect(new RectF((int)Math.ceil(14 * screenDensity),(int)Math.ceil(54 * screenDensity) ,screenWidth - (int)Math.ceil(14 * screenDensity),(int)Math.ceil(122 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                //Draw game status
                paint.setTypeface(thin);
                paint.setTextSize((float) Math.ceil(26 * screenDensity));
                paint.setColor(Color.argb(255, 48, 55, 131));
                if(gameStatusRow2 != "")
                {
                    canvas.drawText(gameStatus , screenWidth/2, (float) Math.ceil(83 * screenDensity), paint);
                    canvas.drawText(gameStatusRow2 , screenWidth/2, (float) Math.ceil(113 * screenDensity), paint);
                }
                else
                {
                    canvas.drawText(gameStatus , screenWidth/2, (float) Math.ceil(98 * screenDensity), paint);
                }

                if(countdownToStart < 3000)
                {
                    paint.setColor(Color.argb(150, 0, 0, 0));
                    canvas.drawRect(0,0,screenWidth,screenHeight,paint);

                    paint.setTypeface(plain);
                    paint.setTextSize((float) Math.ceil(60 * screenDensity));
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText(gameStatus , screenWidth/2, screenHeight/2, paint);
                }

                if(isTutorial1Show)
                {
                    paint.setColor(Color.argb(150, 0, 0, 0));
                    canvas.drawRect(0,0,screenWidth,screenHeight,paint);

                    paint.setColor(Color.argb(255, 0, 0, 0));
                    canvas.drawBitmap(currentWork.getBitmap(), currentWork.getPosX(), currentWork.getPosY(), paint );

                    canvas.drawBitmap(fingerDraw, currentWork.getPosX() + currentWork.getBitmap().getWidth()/2, currentWork.getPosY() + currentWork.getBitmap().getHeight()/2 + tutorialPosy, paint );

                    //Draw message box
                    paint.setColor(Color.argb(255,211,229,245));
                    canvas.drawRoundRect(new RectF((int)Math.ceil(10 * screenDensity),screenHeight - (int)Math.ceil(200 * screenDensity) ,screenWidth - (int)Math.ceil(10 * screenDensity),screenHeight - (int)Math.ceil(76 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                    paint.setColor(Color.argb(255,255,255,255));
                    canvas.drawRoundRect(new RectF((int)Math.ceil(14 * screenDensity),screenHeight - (int)Math.ceil(196 * screenDensity) ,screenWidth - (int)Math.ceil(14 * screenDensity),screenHeight - (int)Math.ceil(84 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(26 * screenDensity));
                    paint.setColor(Color.argb(255, 48, 55, 131));
                    canvas.drawText("Assign the stack of work" , screenWidth/2, screenHeight - (int)Math.ceil(160 * screenDensity), paint);
                    canvas.drawText("by dragging it from the" , screenWidth/2, screenHeight - (int)Math.ceil(130 * screenDensity), paint);
                    canvas.drawText("top of screen to your chosen worker" , screenWidth/2, screenHeight - (int)Math.ceil(100 * screenDensity), paint);
                }
            }
            else if(isGameOver)
            {
                //Draw message box
                paint.setColor(Color.argb(255,211,229,245));
                canvas.drawRoundRect(new RectF((int)Math.ceil(10 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) ,screenWidth - (int)Math.ceil(10 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(300 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                paint.setColor(Color.argb(255,255,255,255));
                canvas.drawRoundRect(new RectF((int)Math.ceil(14 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(4 * screenDensity) ,screenWidth - (int)Math.ceil(14 * screenDensity), (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(292 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                //Draw gameover Text
                canvas.drawBitmap(gameOverBmp, screenWidth/2 - gameOverBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) - (int)Math.ceil(30 * screenDensity), paint);

                //Draw gameover message
                paint.setTypeface(thin);
                paint.setColor(Color.argb(255, 48, 55, 131));
                paint.setTextSize((float) Math.ceil(25 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("Your colleagues are overworked", screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(50 * screenDensity), paint);

                //Draw Highscore
                paint.setTypeface(plain);
                paint.setColor(Color.argb(255, 48, 55, 131));
                paint.setTextSize((float) Math.ceil(26 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                if(elapsedTime/1000 > highscore)
                    canvas.drawText("Highscore : " + elapsedTime/1000, screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(100 * screenDensity), paint);
                else
                    canvas.drawText("Highscore : " + highscore, screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(100 * screenDensity), paint);

                //Draw currentscore
                paint.setTextSize((float) Math.ceil(26 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("Current : " + elapsedTime/1000, screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(135 * screenDensity), paint);

                canvas.drawBitmap(gameOverWorkerBmp, screenWidth/2 - gameOverWorkerBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(170 * screenDensity), paint);

                //Draw continue button
                canvas.drawBitmap(continueBmp, screenWidth/2 - continueBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(320 * screenDensity), paint);
            }
//            else if(!isStart)
//            {
//                paint.setTextSize((float) Math.ceil(30 * screenDensity));
//                paint.setColor(Color.argb(255, 255, 255, 255));
//
//                canvas.drawText("Tap to start", screenWidth / 2, screenHeight / 2, paint);
//
//                canvas.drawBitmap(startPageBmp, null, new Rect(0,0,screenWidth,screenHeight),paint);
//            }

            //Draw debug resolution
//            paint.setTextAlign(Paint.Align.LEFT);
//            paint.setTextSize((float) Math.ceil(10 * screenDensity));
//            canvas.drawText(screenHeight + " " + screenWidth, (float) Math.ceil(5 * screenDensity), (float) Math.ceil(10 * screenDensity), paint);

            //Unlocking the canvas
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int eventAction = event.getAction();

        switch (eventAction) {
            case MotionEvent.ACTION_DOWN:
                //Check when touch happen, detect collision with current document stack
                if(isStart)
                {
                    if (event.getX(0) > currentWork.getPosX() - (float) Math.ceil(20 * screenDensity) && event.getY(0) > currentWork.getPosY() && event.getX(0) < currentWork.getPosX() + currentWork.getBitmap().getWidth() + (float) Math.ceil(20 * screenDensity) && event.getY(0) < currentWork.getPosY() + currentWork.getBitmap().getHeight()) {
                        //Set dragging to true
                        if(!isTutorial1Show && !isTutorial2Show && !isTutorial3Show)
                        {
                            currentWork.isDragging = true;
                            currentWork.setBitmap(worksPullBmps.get(currentWork.getDocumentCount()));
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                //Move current document stack if touch move
                if(isStart)
                {
                    if(currentWork.isDragging)
                    {
                        currentWork.setPosX(event.getX(0) - currentWork.getBitmap().getWidth()/2);
                        currentWork.setPosY(event.getY(0) - currentWork.getBitmap().getHeight()/2);
                    }

                    for(int i = 0; i < workers.size(); i++)
                    {
                        if (event.getX(0) > workers.get(i).getX() && event.getY(0) > workers.get(i).getY() && event.getX(0) < workers.get(i).getX() + workers.get(i).getWidth() && event.getY(0) < workers.get(i).getY() + workers.get(i).getHeight() && currentWork.isDragging) {
                            if(!workers.get(i).isRepeat)
                                workers.get(i).isHighlighted = true;
                        }
                        else
                        {
                            workers.get(i).isHighlighted = false;
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if(!isGameOver && isStart)
                {
                    if(!isTutorial1Show && !isTutorial2Show && !isTutorial3Show)
                    {
                        //Check when touch release, detect collision with workers, assign it
                        for (int i = 0; i < workers.size(); i++) {
                            if (event.getX(0) > workers.get(i).getX() && event.getY(0) > workers.get(i).getY() && event.getX(0) < workers.get(i).getX() + workers.get(i).getWidth() && event.getY(0) < workers.get(i).getY() + workers.get(i).getHeight() && currentWork.isDragging && !workers.get(i).isDie) {
                                if (!workers.get(i).isRepeat || calculateAliveWorker() == 1) {
                                    if (workers.get(i).getDocumentCount() + currentWork.documentCount <= 10) {
                                        soundPool.play(4, 0.1f, 0.1f, 2, 0, 1.0f);
                                        workers.get(i).increaseDocumentCount(currentWork.documentCount);

                                        if (workers.get(i).getDocumentCount() >= 7) {
                                            gameStatus = workers.get(i).name + " " + workers.get(i).nameRow2 + " is feeling bad";
                                            gameStatusRow2 = "";

                                            soundPool.play(6, 0.1f, 0.1f, 1, 0, 1.0f);
                                        } else {
                                            boolean anybored = false;

                                            for (int j = 0; j < workers.size(); j++) {
                                                if (workers.get(j).getDocumentCount() <= 0) {
                                                    gameStatus = workers.get(j).name + " " + workers.get(j).nameRow2 + " is feeling bored";
                                                    gameStatusRow2 = "";
                                                    anybored = true;
                                                }
                                            }

                                            if (!anybored) {
                                                gameStatus = "You are doing well";
                                                gameStatusRow2 = "";
                                            }
                                        }
                                    } else {
                                        workers.get(i).isDie = true;

                                        gameStatus = workers.get(i).name + " " + workers.get(i).nameRow2 + " is exhausted";
                                        gameStatusRow2 = "";

                                        if(!hasTutorial2Show)
                                        {
                                            isTutorial2Show = true;
                                            hasTutorial2Show = true;
                                        }

                                        soundPool.play(5, 0.1f, 0.1f, 1, 0, 1.0f);
                                        //                                isGameOver = true;
                                    }

                                    //remove current works
                                    currentWorks.remove(0);

                                    //change current work
                                    currentWork = currentWorks.get(0);

                                    //add new works to queue
                                    Random rd = new Random();
                                    int documentCount = rd.nextInt(4) + 2;

                                    Works temp = new Works(worksBmps.get(documentCount + 1), documentCount + 1);

                                    currentWorks.add(temp);

                                    //reset document timer
                                    documentTime = 0;

                                    workers.get(i).isHighlighted = false;
                                    for (int j = 0; j < workers.size(); j++) {
                                        if (j != i)
                                            workers.get(j).isRepeat = false;
                                    }
                                    workers.get(i).isRepeat = true;
                                } else {
                                    gameStatus = "You can't assign work";
                                    gameStatusRow2 = "to a same person consecutively";

                                    if(!hasTutorial3Show)
                                    {
                                        isTutorial3Show = true;
                                        hasTutorial3Show = true;
                                    }
                                }
                            }
                        }
                        //Set dragging to false
                        currentWork.isDragging = false;
                        //Update new current work to correct location
                        currentWork.setPosX(currentWork.posXOriginal);
                        currentWork.setPosY(currentWork.posYOriginal);
                        currentWork.setBitmap(worksBmps.get(currentWork.getDocumentCount()));

                        boolean stillAlive = false;
                        for (int i = 0; i < workers.size(); i++) {
                            if (!workers.get(i).isDie)
                                stillAlive = true;
                        }

                        if (!stillAlive) {
                            isGameOver = true;
                            gameOverY = 0.0f;
                        }
                    }
                    else
                    {
                        isTutorial1Show = false;
                        isTutorial2Show = false;
                        isTutorial3Show = false;
                    }
                }
                else if(isGameOver)
                {
                    returnScore();
//                    init(highscore);
                }
//                else if(!isStart)
//                {
//                    isStart = true;
//                }
                break;
        }

        return true;
    }

    public long returnScore()
    {
        return elapsedTime/1000;
    }

    int calculateAliveWorker()
    {
        int aliveWorker = 0;

        for(int i = 0; i < workers.size(); i++)
        {
            if(!workers.get(i).isDie)
                aliveWorker++;
        }

        return aliveWorker;
    }

    void updateFrame(int frameNumber)
    {
        switch (frameNumber)
        {
            case 0:bossFrame = 0; break;
            case 1:bossFrame = 1; break;
            case 2:break;
            case 3:break;
            case 4:break;
            case 5:break;
            case 6:break;
            case 7:break;
            case 8:break;
            case 9:break;
            case 10:break;
            case 11:break;
            case 12:break;
            case 13:break;
            case 14:break;
            case 15:break;
            case 16:break;
            case 17:break;
            case 18:break;
            case 19:break;
            case 20:break;
            case 21:break;
            case 22:break;
            case 23:break;
        }
    }

    //game pause
    public void pause() {
        playing = false;
        try {
            gameThread.join();
            soundPool.autoPause();
        } catch (InterruptedException e) {
        }
    }

    //game resume
    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
        soundPool.autoResume();
    }

    //worker class
    private class Worker
    {
        float WORKING_SPEED_BORED = 0.5f;
        float WORKING_SPEED_NORMAL = 0.3f;
        float WORKING_SPEED_TENSION = 0.2f;

        String name;
        String nameRow2;

        Bitmap bitmap;

        int tableFrame;

        int face;
        int hair;
        int body;
        int hasGlass;
        int glasses;

        float posX;
        float posY;

        int width;
        int height;

        int documentCount;
        float workingTimeLeft;
        float workingTimeMax;
        float workingSpeed;

        boolean isHighlighted;
        boolean isDie;
        boolean isRepeat;

        Emotion emotion;

        public Worker(Bitmap bitmap, float posX, float posY, int width, int height)
        {
            this.bitmap = bitmap;

            this.posX = posX;
            this.posY = posY;

            this.width = width;
            this.height = height;

            Random rd = new Random();

            documentCount = rd.nextInt(5) + 1;
            workingTimeLeft = 1000.0f;
            workingTimeMax = 1000.0f;
            workingSpeed = 0.5f;

            emotion = Emotion.BORED;

            isHighlighted = false;
            isDie = false;
            isRepeat = false;

            tableFrame = rd.nextInt(2);
            face = rd.nextInt(3);
            hair = rd.nextInt(54);
            body = rd.nextInt(6);
            glasses = rd.nextInt(3);
            hasGlass = rd.nextInt(2);

            randomName();
        }

        public Bitmap getBitmap() {
            return bitmap;
        }

        public float getX() {
            return posX;
        }

        public float getY() {
            return posY;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public int getDocumentCount() {
            return documentCount;
        }

        public void increaseDocumentCount (int documentCount) {

            if(this.documentCount + documentCount <= 10)
                this.documentCount += documentCount;
            else
                this.documentCount = 10;

            checkEmotion();
        }

        public void checkEmotion()
        {
            switch (this.documentCount)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                    setEmotion(Emotion.BORED); break;
                case 4:
                case 5:
                case 6:
                    setEmotion(Emotion.NORMAL); break;
                case 7:
                case 8:
                case 9:
                case 10:
                    setEmotion(Emotion.TENSION); break;
            }
        }

        public Emotion getEmotion() {
            return emotion;
        }

        public void setEmotion(Emotion emotion) {
            this.emotion = emotion;
            switch (this.emotion) {
                case BORED:
                    workingSpeed = WORKING_SPEED_BORED;
                    break;
                case NORMAL:
                    workingSpeed = WORKING_SPEED_NORMAL;
                    break;
                case TENSION:
                    workingSpeed = WORKING_SPEED_TENSION;
                    break;
            }
        }

        public float getWorkingTimeLeft() {
            return workingTimeLeft;
        }

        public void doingWork(long currentTime) {
            if(this.workingTimeLeft > 0.0f && this.documentCount > 0)
            {
                this.workingTimeLeft -= currentTime * workingSpeed;
                if(this.workingTimeLeft <= 0.0f)
                {
                    this.documentCount -= 1;
                    this.workingTimeLeft = 1000.0f;

                    checkEmotion();
                }
            }
        }

        public void setWorkingTimeLeft(float workingTimeLeft) {
            this.workingTimeLeft = workingTimeLeft;
        }

        public float getWorkingTimeMax() {
            return workingTimeMax;
        }

        public void setWorkingTimeMax(float workingTimeMax) {
            this.workingTimeMax = workingTimeMax;
        }

        public float getWorkingTimePercentage() {
            return workingTimeLeft / workingTimeMax;
        }

        public void updateFrame(int frameNumber)
        {
            switch (frameNumber)
            {
                case 0:break;
                case 1:break;
                case 2:
                    if(tableFrame == 0)
                        tableFrame = 1;
                    else
                        tableFrame = 0;
                    break;
                case 3:break;
                case 4:break;
                case 5:
                    if(tableFrame == 0)
                        tableFrame = 1;
                    else
                        tableFrame = 0;
                    break;
                case 6:break;
                case 7:break;
                case 8:
                    if(tableFrame == 0)
                        tableFrame = 1;
                    else
                        tableFrame = 0;
                    break;
                case 9:break;
                case 10:break;
                case 11:
                    if(tableFrame == 0)
                        tableFrame = 1;
                    else
                        tableFrame = 0;
                    break;
                case 12:break;
                case 13:break;
                case 14:
                    if(tableFrame == 0)
                        tableFrame = 1;
                    else
                        tableFrame = 0;
                    break;
                case 15:break;
                case 16:break;
                case 17:
                    if(tableFrame == 0)
                        tableFrame = 1;
                    else
                        tableFrame = 0;
                    break;
                case 18:break;
                case 19:break;
                case 20:
                    if(tableFrame == 0)
                        tableFrame = 1;
                    else
                        tableFrame = 0;
                    break;
                case 21:break;
                case 22:break;
                case 23:
                    if(tableFrame == 0)
                        tableFrame = 1;
                    else
                        tableFrame = 0;
                    break;
            }
        }

        private void randomName()
        {
            Random rd = new Random();
            int randomNumber = rd.nextInt(10);

            //    Axiata - Tan Sri Jamaludin Ibrahim
            //    Celcom - Michael Kuehner
            //    XL - Dian Siswarini
            //    Dialog - Supun Weerasinghe
            //    Smart - Thomas Hundt
            //    Ncell - Suren J. Amarasekera
            //    Robi - Mahtab Uddin Ahmed
            //    ADS - Mohd Khairil Abdullah
            if(hair >= 24)
            {
                switch (face)
                {
                    case 0:
                        randomNumber = rd.nextInt(chineseMaleName.length);
                        name = chineseMaleName[randomNumber];
                        nameRow2 = chineseMaleNameRow2[randomNumber];

                        if(randomNumber == 10)
                        {
                            face = 1;
                            hair = 27;
                            glasses = 0;
                            hasGlass = 1;
                        }
                        else if(randomNumber == 11)
                        {
                            face = 1;
                            hair = 29;
                            glasses = 0;
                            hasGlass = 1;
                        }

                        break;
                    case 1:
                        randomNumber = rd.nextInt(malayMaleName.length);
                        name = malayMaleName[randomNumber];
                        nameRow2 = malayMaleNameRow2[randomNumber];

                        if(randomNumber == 10)
                        {
                            face = 1;
                            hair = 39;
                            glasses = 0;
                            hasGlass = 0;
                        }
                        else if(randomNumber == 11)
                        {
                            face = 2;
                            hair = 29;
                            glasses = 0;
                            hasGlass = 0;
                        }
                        else if(randomNumber == 12)
                        {
                            face = 0;
                            hair = 39;
                            glasses = 0;
                            hasGlass = 0;
                        }
                        else if(randomNumber == 13)
                        {
                            face = 0;
                            hair = 38;
                            glasses = 0;
                            hasGlass = 1;
                        }
                        else if(randomNumber == 14)
                        {
                            face = 0;
                            hair = 29;
                            glasses = 0;
                            hasGlass = 0;
                        }

                        break;
                    case 2:
                        randomNumber = rd.nextInt(indianMaleName.length);
                        name = indianMaleName[randomNumber];
                        nameRow2 = indianMaleNameRow2[randomNumber];

                        if(randomNumber == 10)
                        {
                            face = 2;
                            hair = 35;
                            glasses = 0;
                            hasGlass = 1;
                        }
                        else if(randomNumber == 11)
                        {
                            face = 2;
                            hair = 48;
                            glasses = 0;
                            hasGlass = 1;
                        }

                        break;
                }
            }
            else
            {
                switch (face)
                {
                    case 0:
                        randomNumber = rd.nextInt(chineseFemaleName.length);
                        name = chineseFemaleName[randomNumber];
                        nameRow2 = chineseFemaleNameRow2[randomNumber];
                        break;
                    case 1:
                        randomNumber = rd.nextInt(malayFemaleName.length);
                        name = malayFemaleName[randomNumber];
                        nameRow2 = malayFemaleNameRow2[randomNumber];

                        if(randomNumber == 10)
                        {
                            face = 0;
                            hair = 0;
                            glasses = 2;
                            hasGlass = 1;
                        }
                        break;
                    case 2:
                        randomNumber = rd.nextInt(indianFemaleName.length);
                        name = indianFemaleName[randomNumber];
                        nameRow2 = indianFemaleNameRow2[randomNumber];
                        break;
                }
            }
        }
    }

    private class Works
    {
        Bitmap bitmap;

        int documentCount;

        float posX;
        float posY;

        public float posXOriginal;
        public float posYOriginal;

        public boolean isDragging;

        public Works(Bitmap bitmap, int documentCount)
        {
            this.bitmap = bitmap;
            this.documentCount = documentCount;

            this.posX = screenWidth/2 - this.bitmap.getWidth() / 2;
            this.posXOriginal = screenWidth/2 - this.bitmap.getWidth() / 2;
            this.posY = (float) Math.ceil(170 * screenDensity);
            this.posYOriginal = (float) Math.ceil(170 * screenDensity);

            this.isDragging = false;
        }

        public Bitmap getBitmap()
        {
            return bitmap;
        }

        public void setBitmap(Bitmap bitmap)
        {
            this.bitmap = bitmap;
        }

        public int getDocumentCount()
        {
            return documentCount;
        }

        public float getPosX()
        {
            return posX;
        }

        public void setPosX(float posX)
        {
            this.posX = posX;
        }

        public float getPosY()
        {
            return posY;
        }

        public void setPosY(float posY)
        {
            this.posY = posY;
        }
    }

    public void onDestroy()
    {
        if(startPageBmp != null)
        {
            startPageBmp.recycle();
            startPageBmp = null;
        }
        if(workerBmp != null)
        {
            workerBmp.recycle();
            workerBmp = null;
        }
        if(bg != null)
        {
            bg.recycle();
            bg = null;
        }
        if(nextFrame != null)
        {
            nextFrame.recycle();
            nextFrame = null;
        }
        if(nextBar != null)
        {
            nextBar.recycle();
            nextBar = null;
        }
        if(clock != null)
        {
            clock.recycle();
            clock = null;
        }
        if(bossBmp != null)
        {
            bossBmp.recycle();
            bossBmp = null;
        }
        if(boxBmp != null)
        {
            boxBmp.recycle();
            boxBmp = null;
        }

        if(worksBmps != null)
        {
            while (worksBmps.size() < 0)
            {
                worksBmps.get(0).recycle();
                worksBmps.remove(0);
            }
            worksBmps.clear();
        }
        if(worksPullBmps != null)
        {
            while (worksPullBmps.size() < 0)
            {
                worksPullBmps.get(0).recycle();
                worksPullBmps.remove(0);
            }
            worksPullBmps.clear();
        }
        if(worksSmallBmps != null)
        {
            while (worksSmallBmps.size() < 0)
            {
                worksSmallBmps.get(0).recycle();
                worksSmallBmps.remove(0);
            }
            worksSmallBmps.clear();
        }
        if(table2Frame != null)
        {
            while (table2Frame.size() < 0)
            {
                table2Frame.get(0).recycle();
                table2Frame.remove(0);
            }
            table2Frame.clear();
        }
        if(tableFainted2Frame != null)
        {
            while (tableFainted2Frame.size() < 0)
            {
                tableFainted2Frame.get(0).recycle();
                tableFainted2Frame.remove(0);
            }
            tableFainted2Frame.clear();
        }
        if(boss2Frame != null)
        {
            while (boss2Frame.size() < 0)
            {
                boss2Frame.get(0).recycle();
                boss2Frame.remove(0);
            }
            boss2Frame.clear();
        }
        if(bodyBmps != null)
        {
            while (bodyBmps.size() < 0)
            {
                bodyBmps.get(0).recycle();
                bodyBmps.remove(0);
            }
            bodyBmps.clear();
        }
        if(bodyFaintedBmps != null)
        {
            while (bodyFaintedBmps.size() < 0)
            {
                bodyFaintedBmps.get(0).recycle();
                bodyFaintedBmps.remove(0);
            }
            bodyFaintedBmps.clear();
        }
        if(faceBmps != null)
        {
            while (faceBmps.size() < 0)
            {
                faceBmps.get(0).recycle();
                faceBmps.remove(0);
            }
            faceBmps.clear();
        }
        if(faceRedBmps != null)
        {
            while (faceRedBmps.size() < 0)
            {
                faceRedBmps.get(0).recycle();
                faceRedBmps.remove(0);
            }
            faceRedBmps.clear();
        }
        if(hairBmps != null)
        {
            while (hairBmps.size() < 0)
            {
                hairBmps.get(0).recycle();
                hairBmps.remove(0);
            }
            hairBmps.clear();
        }
        if(glassesBmps != null)
        {
            while (glassesBmps.size() < 0)
            {
                glassesBmps.get(0).recycle();
                glassesBmps.remove(0);
            }
            glassesBmps.clear();
        }

        if(emptyTable != null)
        {
            emptyTable.recycle();
            emptyTable = null;
        }

        if(gameOverBmp != null)
        {
            gameOverBmp.recycle();
            gameOverBmp = null;
        }
        if(gameOverWorkerBmp != null)
        {
            gameOverWorkerBmp.recycle();
            gameOverWorkerBmp = null;
        }
        if(continueBmp != null)
        {
            continueBmp.recycle();
            continueBmp = null;
        }

        if(highlightBmp != null)
        {
            highlightBmp.recycle();
            highlightBmp = null;
        }

        if(boredBmp != null)
        {
            boredBmp.recycle();
            boredBmp = null;
        }
        if(normalBmp != null)
        {
            normalBmp.recycle();
            normalBmp = null;
        }
        if(tensionBmp != null)
        {
            tensionBmp.recycle();
            tensionBmp = null;
        }

        if(progressBmp != null)
        {
            progressBmp.recycle();
            progressBmp = null;
        }
        if(boredProgressBmp != null)
        {
            boredProgressBmp.recycle();
            boredProgressBmp = null;
        }
        if(normalProgressBmp != null)
        {
            normalProgressBmp.recycle();
            normalProgressBmp = null;
        }
        if(tensionProgressBmp != null)
        {
            tensionProgressBmp.recycle();
            tensionProgressBmp = null;
        }

        if(boredExpressionBmp != null)
        {
            boredExpressionBmp.recycle();
            boredExpressionBmp = null;
        }
        if(normalExpressionBmp != null)
        {
            normalExpressionBmp.recycle();
            normalExpressionBmp = null;
        }
        if(tensionExpressionBmp != null)
        {
            tensionExpressionBmp.recycle();
            tensionExpressionBmp = null;
        }
        if(faintedExpressionBmp != null)
        {
            faintedExpressionBmp.recycle();
            faintedExpressionBmp = null;
        }

        if(fingerDownBmp != null)
        {
            fingerDownBmp.recycle();
            fingerDownBmp = null;
        }
        if(fingerUpBmp != null)
        {
            fingerUpBmp.recycle();
            fingerUpBmp = null;
        }
        if(arrowBmp != null)
        {
            arrowBmp.recycle();
            arrowBmp = null;
        }

        Runtime.getRuntime().gc();
        System.gc();
    }
}
