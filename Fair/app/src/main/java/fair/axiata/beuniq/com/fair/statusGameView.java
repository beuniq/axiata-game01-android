package fair.axiata.beuniq.com.fair;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)

/**
 * Created by beuniq on 25/05/2017.
 */

public class statusGameView extends SurfaceView implements Runnable {

    volatile boolean playing;
    private Thread gameThread = null;

    //These objects will be used for drawing
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;

    private Context thisContext;

    private Float screenDensity;
    private int screenWidth;
    private int screenHeight;

    Typeface plain;
    Typeface thin;

    Bitmap clock;
    Bitmap bg;
    Bitmap gameOverBmp;
    Bitmap gameOverWorkerBmp;
    Bitmap continueBmp;
    Bitmap nextLevelBmp;
    Bitmap workerBmp;
    Bitmap windowBmp;
    Bitmap tutorialArrowBmp;
    Bitmap tutorialArrowUpBmp;
    List<Bitmap> selectionIcon;

    long currentTime;
    long elapsedTime;
    float timeMultiplier;
    boolean isGameOver;
    boolean isStart;
    boolean isNextStage;
    long roundAnswerTimer;
    long answerTimer;
    long countdownToStart;
    long highscore;
    long nextBonus;
    long totalBonus;

    int stepUsed;
    int totalLevelStep;
    int convertedSquare;

    float gameOverY;

    String gameStatus;

    long frameTime;
    long frameTimeMax;
    int fps;
    int frameNumber;

    int row = 12;
    int column = 15;

    float squareSize = 20.0f;

    boolean[][] gridA = {{true,true,false},{false,true,true},{false,false,false}};
    int [][] gridB = {  {1,1,3},
            {3,1,1},
            {4,3,2} };
    boolean[][] gridC = {{false,false,false},{false,false,false},{false,false,false}};

    SoundPool soundPool;
    boolean inGame;
    boolean soundOn;

    List<numberDisplay> numberDisplayList;

    int color0 = Color.argb(255,250,52,45);
    int color1 = Color.argb(255,255,130,60);
    int color2 = Color.argb(255,52,152,219);
    int color3 = Color.argb(255,28,215,130);
    int color4 = Color.argb(255,255,220,62);
    int color5 = Color.argb(255,255,104,162);

    int color0highlight = Color.argb(255,146,5,0);
    int color1highlight = Color.argb(255,157,56,0);
    int color2highlight = Color.argb(255,0,58,97);
    int color3highlight = Color.argb(255,0,100,54);
    int color4highlight = Color.argb(255,137,91,0);
    int color5highlight = Color.argb(255,161,0,62);

    String[] selectionText = {"Agile Mindset", "Collaborative","Customer Obsessed","Data Driven","Experimenting","Fast Execution","Hyper Aware","Informed decision","Innovative","Keep Up","Persistent","Simplicity","Social Media","Team Work"};
    String[] selectionTextLine2 = {"", "","","","","","","Making","","","","","Savvy",""};

    int selectedColor = 0;
    int[] selectionList;

    int quoteNumber;

    String[] nextStageQuoteLine1 = {"A Data Driven individual will find","Teamwork gives us the ability","Being Hyper Aware helps us","Fast Execution lets us catch","An Agile Mindset would","Simplicity allows for easier","Experimenting keeps us open to","Informed Decision Making"};
    String[] nextStageQuoteLine2 = {"it easier to connect the dots","to leverage on our shared strengths","to adapt and act according to","opportunities as they come","allow us to embrace change easier","understanding, execution and","better ways of doing things","helps you make better choices"};
    String[] nextStageQuoteLine3 = {"in trends, actions and results to","and counter our personal shortcomings.","new circumstances and information.","and improves upon our work efficiency.","in order to excel in trying times.","efficiency in our daily lives.","and embracing creative approaches.","and be a better leader."};
    String[] nextStageQuoteLine4 = {"improve themselves or the organisation.","","","","","","",""};

    boolean isTutorial1Show;
    boolean isTutorial2Show;
    boolean isTutorial3Show;

    boolean hasTutorialShow;

    float tutorialX, tutorialY;

//    int nextColor = 3;

    public statusGameView(Context context, Float density) {
        super(context);

        thisContext = context;

        screenDensity = density;

        //initializing drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();

        if (Build.VERSION.SDK_INT >= 11) {
            Point size = new Point();
            try {
                ((Activity) getContext()).getWindowManager().getDefaultDisplay().getRealSize(size);
                screenWidth = size.x;
                screenHeight = size.y;
            } catch (NoSuchMethodError e) {
                screenHeight = display.getHeight();
            }

        } else {
            DisplayMetrics metrics = new DisplayMetrics();
            ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenWidth = display.getWidth();
            screenHeight = display.getHeight();
        }

        soundPool = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        soundPool.load(thisContext, R.raw.bgm3,1);
        soundPool.load(thisContext, R.raw.button3,1);
        soundPool.load(thisContext, R.raw.win3,1);
        soundPool.load(thisContext, R.raw.lose3,1);

//        init();

//        calculate();

    }

    void buildInitialGrid()
    {
        gridA = new boolean[row][column];
        gridB = new int[row][column];
        gridC = new boolean[row][column];

        selectionList = new int[6];

        Random rd = new Random();

        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < column; j++)
            {
                gridA[i][j] = false;
                gridC[i][j] = false;

                gridB[i][j] = rd.nextInt(6);
            }
        }

        gridA[0][0] = true;

        while(gridB[0][0] == gridB[0][1])
        {
            gridB[0][1] = rd.nextInt(6);
        }

        while(gridB[0][0] == gridB[1][0])
        {
            gridB[1][0] = rd.nextInt(6);
        }

        selectedColor = gridB[0][0];

        int counter = 0;
        while (counter < 6)
        {
            boolean isRepeat = false;
            selectionList[counter] = rd.nextInt(14);

            for(int i = 0; i < counter; i++)
            {
                if(selectionList[counter] == selectionList[i])
                {
                    isRepeat = true;
                }
            }

            if(!isRepeat)
            {
                counter++;
            }
        }

        Log.d("grid built","True");
    }

    void calculate(int nextColor)
    {
        soundPool.play(2, 1.0f, 1.0f, 1, 0, 1.0f);
        boolean isFound;

        do{
            isFound = false;

            for(int i = 0; i < row; i++)
            {
                for(int j = 0; j < column; j++)
                {
                    if(gridA[i][j])
                    {
                        if(i + 1 < row)
                        {
                            if(!gridA[i+1][j] && gridB[i+1][j] == nextColor)
                            {
                                gridC[i+1][j] = true;

                                isFound = true;
                            }
                        }

                        if(j + 1 < column)
                        {
                            if(!gridA[i][j+1] && gridB[i][j+1] == nextColor)
                            {
                                gridC[i][j+1] = true;

                                isFound = true;
                            }
                        }

                        if(i != 0)
                        {
                            if(!gridA[i-1][j] && gridB[i-1][j] == nextColor)
                            {
                                gridC[i-1][j] = true;

                                isFound = true;
                            }
                        }

                        if(j != 0)
                        {
                            if(!gridA[i][j - 1] && gridB[i][j-1] == nextColor)
                            {
                                gridC[i][j - 1] = true;

                                isFound = true;
                            }
                        }

                        gridC[i][j] = true;
                    }
                }
            }

            for(int i = 0; i < row; i++)
            {
                for(int j = 0; j < column; j++)
                {
                    gridA[i][j] = gridC[i][j];
                }
            }
        }while (isFound);

        boolean isComplete = true;
        convertedSquare = 1;
        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < column; j++)
            {
                if(gridA[i][j])
                {
                    gridB[i][j] = nextColor;
                    convertedSquare++;
                }
                else
                {
                    isComplete = false;
                }
            }
        }

        Log.d("Converted square", String.valueOf(convertedSquare));

        if(isComplete)
        {
            soundPool.play(3, 1.0f, 1.0f, 1, 0, 1.0f);
            isNextStage = true;
            gameOverY = 0.0f;

            nextBonus = (totalLevelStep - stepUsed) * ((50 - totalLevelStep) / 5);
            totalBonus += nextBonus;
        }

        if(answerTimer > roundAnswerTimer / 2)
        {
            numberDisplay temp = new numberDisplay("+1s",screenWidth / 2,screenHeight - (2 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(30 * screenDensity) - (float) Math.ceil(35 * screenDensity));
            numberDisplayList.add(temp);
        }
        else
        {
            numberDisplay temp = new numberDisplay("+0s",screenWidth / 2,screenHeight - (2 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(30 * screenDensity) - (float) Math.ceil(35 * screenDensity));
            numberDisplayList.add(temp);
        }

        selectedColor = nextColor;
        answerTimer = roundAnswerTimer;
        stepUsed += 1;
        if(stepUsed > totalLevelStep)
        {
            soundPool.play(4, 1.0f, 1.0f, 1, 0, 1.0f);
            isGameOver = true;
            isNextStage = false;
            gameOverY = 0.0f;
        }
    }

    @Override
    public void run()
    {
        while (playing)
        {
            if(inGame)
            {
                if(!soundOn)
                {
                    soundPool.play(1,0.1f,0.1f,1,-1,1.0f);
                    soundOn = true;
                }

                update();
                draw();
            }
        }
    }

    public void init(long highscore)
    {
//        onDestroy();

        plain = Typeface.createFromAsset(thisContext.getApplicationContext().getAssets(), "fonts/AllerDisplay.ttf");
        thin = Typeface.createFromAsset(thisContext.getApplicationContext().getAssets(), "fonts/Samuel.ttf");

        numberDisplayList = new ArrayList<numberDisplay>();
        selectionIcon = new ArrayList<Bitmap>();

        bg = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.bg_03);
        clock = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_clock);

        gameOverBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.gameover_text);
        gameOverWorkerBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.gameover_icon);
        continueBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.btn_continue_01);

        nextLevelBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.next_stage);
        workerBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.worker_g3);
        windowBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.window_g3);

        tutorialArrowBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.arrow);
        tutorialArrowUpBmp = BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.arrowup);

        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_agile_mindset));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_collaborative));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_customer_obsessed));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_data_driven));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_experimenting));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_fast_execution));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_hyper_aware));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_informed_decision_making));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_innovative));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_keep_up));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_persistent));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_simplicity));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_social_media_savvy));
        selectionIcon.add(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_team_work));

        //initialize in game time counter
        currentTime = System.currentTimeMillis();
        elapsedTime = 0;
        timeMultiplier = 1;
        isGameOver = false;
        isNextStage = false;
        isStart = false;

        this.highscore = highscore;

        isTutorial1Show = false;
        isTutorial2Show = false;
        isTutorial3Show = false;

        if(this.highscore > 0)
        {
            hasTutorialShow = true;
        }
        else
        {
            hasTutorialShow = false;
        }

        countdownToStart = 0;
        roundAnswerTimer = 5000;
        totalLevelStep = 40;
        stepUsed = 0;
        convertedSquare = 1;

        fps = 24;
        frameTimeMax = 1000/fps;
        frameTime = 0;

        buildInitialGrid();
    }

    public void nextStage()
    {
        buildInitialGrid();

        Random rd = new Random();

        quoteNumber = rd.nextInt(8);

        isGameOver = false;
        isStart = false;
        isNextStage = false;

        countdownToStart = 0;

        if(roundAnswerTimer > 2000)
            roundAnswerTimer -= 1000;

        answerTimer = roundAnswerTimer;

        if(totalLevelStep > 25)
            totalLevelStep -= 5;

        stepUsed = 0;
        convertedSquare = 1;
    }

    private void update()
    {
        //update time
        long millis = System.currentTimeMillis();
        long different = (long)((millis - currentTime) * timeMultiplier);
        long differentWithMultiplier = (long)((millis - currentTime) * timeMultiplier);
        currentTime = millis;

        if(!isStart)
        {
            countdownToStart += different;

            if(countdownToStart > 3000)
            {
                isStart = true;
//                countdownToStart = 0;
                gameStatus = "Start";
                answerTimer = roundAnswerTimer;

                if(!hasTutorialShow)
                {
                    isTutorial1Show = true;
                    hasTutorialShow = true;

                    tutorialX = (float) Math.ceil(40 * screenDensity);
                    tutorialY = (float) Math.ceil(150 * screenDensity);
                }

            }
            else
            {
                gameStatus = String.valueOf(3 - (countdownToStart / 1000));
            }
        }

        //do when game is not over and has started
        if(!isGameOver && isStart && !isNextStage && !isTutorial1Show && !isTutorial2Show && !isTutorial3Show)
        {
            elapsedTime += different;

            if(answerTimer > 0)
            {
                answerTimer -= different;
            }
            else
            {
                soundPool.play(4, 1.0f, 1.0f, 1, 0, 1.0f);
                isGameOver = true;
                isNextStage = false;
                gameOverY = 0.0f;
            }
        }

        for(int i = 0; i < numberDisplayList.size(); i++)
        {
            if(numberDisplayList.get(i).posY > screenHeight - (2 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(30 * screenDensity) - (float) Math.ceil(35 * screenDensity) - (float) Math.ceil(24 * screenDensity))
                numberDisplayList.get(i).moveUp();
            else
                numberDisplayList.remove(i);
        }

        if(isGameOver)
        {
            if(gameOverY < 150.0f)
                gameOverY += 24.0f;
            else
                gameOverY = 150.0f;
        }

        if(isNextStage)
        {
            if(gameOverY < 150.0f)
                gameOverY += 24.0f;
            else
                gameOverY = 150.0f;
        }

        //update frame
        if(frameTime > frameTimeMax)
        {
            if(frameNumber + 1 < fps - 1)
            {
                frameNumber++;
            }
            else
            {
                frameNumber = 0;
            }

            frameTime = 0;
        }
        else
        {
            frameTime += different;
        }
    }

    private void draw() {
        //checking if surface is valid
        if (surfaceHolder.getSurface().isValid()) {
            //locking the canvas
            canvas = surfaceHolder.lockCanvas();

            //Set text color
            paint.setColor(Color.argb(255, 76, 76, 76));
            paint.setTextSize((float) Math.ceil(16 * screenDensity));
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTypeface(plain);

            //Draw background
            canvas.drawBitmap(bg, null, new Rect(0,0,screenWidth,screenHeight),paint);

            if(!isGameOver && !isNextStage)
            {
                canvas.drawBitmap(workerBmp, (float) Math.ceil(10 * screenDensity), (float) Math.ceil(50 * screenDensity), paint);
                canvas.drawBitmap(windowBmp, screenWidth - windowBmp.getWidth() * 0.8f, (float) Math.ceil(40 * screenDensity), paint);

                //draw grid
                for(int i = 0; i < row; i++)
                {
                    for(int j = 0; j < column; j++)
                    {
                        switch (gridB[i][j])
                        {
                            case 0: paint.setColor(color0); break;
                            case 1: paint.setColor(color1); break;
                            case 2: paint.setColor(color2); break;
                            case 3: paint.setColor(color3); break;
                            case 4: paint.setColor(color4); break;
                            case 5: paint.setColor(color5); break;
                        }

                        canvas.drawRect(screenWidth/2 - (7.5f * (float) Math.ceil(squareSize * screenDensity)) + (j * (float) Math.ceil(squareSize * screenDensity)),
                                        screenHeight/2 + (i * (float) Math.ceil(squareSize * screenDensity)) - (float) Math.ceil(170 * screenDensity),
                                        screenWidth/2 - 7.5f * (float) Math.ceil(squareSize * screenDensity) + ((j + 1) * (float) Math.ceil((squareSize) * screenDensity)),
                                        screenHeight/2 + ((i + 1) * (float) Math.ceil(squareSize * screenDensity)) - (float) Math.ceil(170 * screenDensity),
                                        paint);
                    }
                }

                //draw click timer
                paint.setColor(Color.argb(255,255,255,255));
                canvas.drawRoundRect(new RectF(screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                        screenHeight - (2 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(10 * screenDensity) - (float) Math.ceil(45 * screenDensity),
                        screenWidth/2 + 0.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) + (float) Math.ceil(15 * screenDensity),
                        screenHeight - (2 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(10 * screenDensity) - (float) Math.ceil(35 * screenDensity)),
                        (float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                paint.setColor(Color.argb(255,78,255,160));
                canvas.drawRoundRect(new RectF(screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                                screenHeight - (2 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(10 * screenDensity) - (float) Math.ceil(45 * screenDensity),
                                screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(300 * screenDensity * answerTimer / roundAnswerTimer),
                                screenHeight - (2 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(10 * screenDensity) - (float) Math.ceil(35 * screenDensity)),
                        (float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                for(int i = 0; i < numberDisplayList.size(); i++)
                {
                    paint.setTypeface(plain);
                    paint.setTextSize((float) Math.ceil(30 * screenDensity));
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setColor(Color.argb(255, 76, 76, 76));
                    canvas.drawText(numberDisplayList.get(i).text, numberDisplayList.get(i).posX + 6, numberDisplayList.get(i).posY + 6, paint);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText(numberDisplayList.get(i).text, numberDisplayList.get(i).posX, numberDisplayList.get(i).posY, paint);
                }

                //Draw Timer
                paint.setTypeface(plain);
                paint.setTextSize((float) Math.ceil(30 * screenDensity));
                paint.setColor(Color.argb(255, 58, 123, 178));
                canvas.drawBitmap(clock,screenWidth/2 - clock.getWidth() - (float) Math.ceil(22 * screenDensity),(float) Math.ceil(15 * screenDensity),paint);
                canvas.drawText(String.format("%02d", elapsedTime/60/1000) + ":" + String.format("%02d", elapsedTime/1000 % 60) , screenWidth/2 + (float) Math.ceil(20 * screenDensity), (float) Math.ceil(38 * screenDensity), paint);

                //Draw message box
                paint.setColor(Color.argb(255,211,229,245));
                canvas.drawRoundRect(new RectF((int)Math.ceil(80 * screenDensity),(int)Math.ceil(45 * screenDensity) ,screenWidth - (int)Math.ceil(50 * screenDensity),(int)Math.ceil(45 * screenDensity) + (int)Math.ceil(60 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                paint.setColor(Color.argb(255,255,255,255));
                canvas.drawRoundRect(new RectF((int)Math.ceil(84 * screenDensity),(int)Math.ceil(45 * screenDensity) + (int)Math.ceil(4 * screenDensity) ,screenWidth - (int)Math.ceil(54 * screenDensity), (int)Math.ceil(45 * screenDensity) + (int)Math.ceil(52 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                //step display
                paint.setTypeface(thin);
                paint.setTextSize((float) Math.ceil(18 * screenDensity));
                paint.setTextAlign(Paint.Align.LEFT);
                paint.setColor(Color.argb(255, 58, 123, 178));
                canvas.drawText("Move: " + stepUsed + " / " + totalLevelStep,(float) Math.ceil(90 * screenDensity), (float) Math.ceil(80 * screenDensity), paint);

                //percent display
                paint.setTypeface(thin);
                paint.setTextSize((float) Math.ceil(18 * screenDensity));
                paint.setTextAlign(Paint.Align.RIGHT);
                paint.setColor(Color.argb(255, 58, 123, 178));
                canvas.drawText("Progress: " + ((convertedSquare * 100)/(row * column)) + " / 100", screenWidth - (float) Math.ceil(60 * screenDensity), (float) Math.ceil(80 * screenDensity), paint);

                if(isTutorial1Show) {
                    paint.setColor(Color.argb(150, 0, 0, 0));
                    canvas.drawRect(0, 0, screenWidth, screenHeight, paint);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                }

                //draw button
                paint.setTypeface(thin);
                paint.setTextSize((float) Math.ceil(13 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);

                paint.setColor(color0);
                canvas.drawRoundRect(new RectF(screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity),
                        screenWidth/2 - 1.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                        screenHeight - ((1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity)),
                        (float)Math.ceil(3 * screenDensity),(float)Math.ceil(3 * screenDensity),paint);
                if(selectedColor != 0)
                    paint.setColor(Color.WHITE);
                else
                    paint.setColor(color0highlight);
                canvas.drawText(selectionText[selectionList[0]],
                        screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(70 * screenDensity), paint);
                canvas.drawText(selectionTextLine2[selectionList[0]],
                        screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(85 * screenDensity), paint);
                if(selectedColor == 0)
                    paint.setColorFilter(new LightingColorFilter(color0highlight,0));
                canvas.drawBitmap(selectionIcon.get(selectionList[0]),
                        screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(3 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity),paint);
                paint.setColorFilter(null);

                paint.setColor(color1);
                canvas.drawRoundRect(new RectF(screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity),
                        screenWidth/2 - 0.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)),
                        screenHeight - (1 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity)),
                        (float)Math.ceil(3 * screenDensity),(float)Math.ceil(3 * screenDensity),paint);
                if(selectedColor != 1)
                    paint.setColor(Color.WHITE);
                else
                    paint.setColor(color1highlight);
                canvas.drawText(selectionText[selectionList[1]],
                        screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(70 * screenDensity), paint);
                canvas.drawText(selectionTextLine2[selectionList[1]],
                        screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(85 * screenDensity), paint);
                if(selectedColor == 1)
                    paint.setColorFilter(new LightingColorFilter(color1highlight,0));
                canvas.drawBitmap(selectionIcon.get(selectionList[1]),
                        screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(3 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity),paint);
                paint.setColorFilter(null);

                paint.setColor(color2);
                canvas.drawRoundRect(new RectF(screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity),
                        screenWidth/2 + 0.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) + (float) Math.ceil(15 * screenDensity),
                        screenHeight - (1 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity)),
                        (float)Math.ceil(3 * screenDensity),(float)Math.ceil(3 * screenDensity),paint);
                if(selectedColor != 2)
                    paint.setColor(Color.WHITE);
                else
                    paint.setColor(color2highlight);
                canvas.drawText(selectionText[selectionList[2]],
                        screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(70 * screenDensity), paint);
                canvas.drawText(selectionTextLine2[selectionList[2]],
                        screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(85 * screenDensity), paint);
                if(selectedColor == 2)
                    paint.setColorFilter(new LightingColorFilter(color2highlight,0));
                canvas.drawBitmap(selectionIcon.get(selectionList[2]),
                        screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(3 * screenDensity),
                        screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity),paint);
                paint.setColorFilter(null);

                paint.setColor(color3);
                canvas.drawRoundRect(new RectF(screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                        screenWidth/2 - 1.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                        screenHeight - (0 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity)),
                        (float)Math.ceil(3 * screenDensity),(float)Math.ceil(3 * screenDensity),paint);
                if(selectedColor != 3)
                    paint.setColor(Color.WHITE);
                else
                    paint.setColor(color3highlight);
                canvas.drawText(selectionText[selectionList[3]],
                        screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(70 * screenDensity), paint);
                canvas.drawText(selectionTextLine2[selectionList[3]],
                        screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(85 * screenDensity), paint);
                if(selectedColor == 3)
                    paint.setColorFilter(new LightingColorFilter(color3highlight,0));
                canvas.drawBitmap(selectionIcon.get(selectionList[3]),
                        screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(3 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),paint);
                paint.setColorFilter(null);

                paint.setColor(color4);
                canvas.drawRoundRect(new RectF(screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                        screenWidth/2 - 0.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)),
                        screenHeight - (0 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity)),
                        (float)Math.ceil(3 * screenDensity),(float)Math.ceil(3 * screenDensity),paint);
                if(selectedColor != 4)
                    paint.setColor(Color.WHITE);
                else
                    paint.setColor(color4highlight);
                canvas.drawText(selectionText[selectionList[4]],
                        screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(70 * screenDensity), paint);
                canvas.drawText(selectionTextLine2[selectionList[4]],
                        screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(85 * screenDensity), paint);
                if(selectedColor == 4)
                    paint.setColorFilter(new LightingColorFilter(color4highlight,0));
                canvas.drawBitmap(selectionIcon.get(selectionList[4]),
                        screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(3 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),paint);
                paint.setColorFilter(null);

                paint.setColor(color5);
                canvas.drawRoundRect(new RectF(screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                        screenWidth/2 + 0.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) + (float) Math.ceil(15 * screenDensity),
                        screenHeight - (0 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity)),
                        (float)Math.ceil(3 * screenDensity),(float)Math.ceil(3 * screenDensity),paint);
                if(selectedColor != 5)
                    paint.setColor(Color.WHITE);
                else
                    paint.setColor(color5highlight);
                canvas.drawText(selectionText[selectionList[5]],
                        screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(70 * screenDensity), paint);
                canvas.drawText(selectionTextLine2[selectionList[5]],
                        screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(45 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(85 * screenDensity), paint);
                if(selectedColor == 5)
                    paint.setColorFilter(new LightingColorFilter(color5highlight,0));
                canvas.drawBitmap(selectionIcon.get(selectionList[5]),
                        screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(3 * screenDensity),
                        screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),paint);
                paint.setColorFilter(null);

                if(isTutorial1Show)
                {
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawBitmap(tutorialArrowBmp, tutorialX, tutorialY - tutorialArrowBmp.getHeight(), paint);

                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(25 * screenDensity));
                    paint.setTextAlign(Paint.Align.LEFT);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText("You start from here", tutorialX - tutorialArrowBmp.getWidth(), tutorialY - tutorialArrowBmp.getHeight() - (float) Math.ceil(10 * screenDensity), paint);

                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(25 * screenDensity));
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText("Your mission is to change your colour", screenWidth/2, screenHeight/2 + (float) Math.ceil(30 * screenDensity), paint);
                    canvas.drawText("to match with adjacent colour", screenWidth/2, screenHeight/2 + (float) Math.ceil(60 * screenDensity), paint);
                    canvas.drawText("by clicking the button below", screenWidth/2, screenHeight/2 + (float) Math.ceil(90 * screenDensity), paint);
                }

                if(isTutorial2Show)
                {
                    paint.setColor(Color.argb(150, 0, 0, 0));
                    canvas.drawRect(0,0,screenWidth,screenHeight,paint);

                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawBitmap(tutorialArrowUpBmp, tutorialX, tutorialY - tutorialArrowBmp.getHeight(), paint);

                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(25 * screenDensity));
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText("This is your step used and progress achieved", screenWidth / 2, tutorialY + (float) Math.ceil(30 * screenDensity), paint);
                    canvas.drawText("Once you run out of step, the game end", screenWidth / 2, tutorialY + (float) Math.ceil(60 * screenDensity), paint);

                    //Draw message box
                    paint.setColor(Color.argb(255,211,229,245));
                    canvas.drawRoundRect(new RectF((int)Math.ceil(80 * screenDensity),(int)Math.ceil(45 * screenDensity) ,screenWidth - (int)Math.ceil(50 * screenDensity),(int)Math.ceil(45 * screenDensity) + (int)Math.ceil(60 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                    paint.setColor(Color.argb(255,255,255,255));
                    canvas.drawRoundRect(new RectF((int)Math.ceil(84 * screenDensity),(int)Math.ceil(45 * screenDensity) + (int)Math.ceil(4 * screenDensity) ,screenWidth - (int)Math.ceil(54 * screenDensity), (int)Math.ceil(45 * screenDensity) + (int)Math.ceil(52 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                    //step display
                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(18 * screenDensity));
                    paint.setTextAlign(Paint.Align.LEFT);
                    paint.setColor(Color.argb(255, 58, 123, 178));
                    canvas.drawText("Move: " + stepUsed + " / " + totalLevelStep,(float) Math.ceil(90 * screenDensity), (float) Math.ceil(80 * screenDensity), paint);

                    //percent display
                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(18 * screenDensity));
                    paint.setTextAlign(Paint.Align.RIGHT);
                    paint.setColor(Color.argb(255, 58, 123, 178));
                    canvas.drawText("Progress: " + ((convertedSquare * 100)/(row * column)) + " / 100", screenWidth - (float) Math.ceil(60 * screenDensity), (float) Math.ceil(80 * screenDensity), paint);
                }

                if(isTutorial3Show)
                {
                    paint.setColor(Color.argb(150, 0, 0, 0));
                    canvas.drawRect(0,0,screenWidth,screenHeight,paint);

                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawBitmap(tutorialArrowBmp, tutorialX, tutorialY - tutorialArrowBmp.getHeight(), paint);

                    paint.setTypeface(thin);
                    paint.setTextSize((float) Math.ceil(25 * screenDensity));
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText("This is the timer for each turn", screenWidth/2, screenHeight/2 + (float) Math.ceil(30 * screenDensity), paint);
                    canvas.drawText("When time runs out, the game end", screenWidth/2, screenHeight/2 + (float) Math.ceil(60 * screenDensity), paint);

                    //draw click timer
                    paint.setColor(Color.argb(255,78,255,160));
                    canvas.drawRoundRect(new RectF(screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity),
                                    screenHeight - (2 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(10 * screenDensity) - (float) Math.ceil(45 * screenDensity),
                                    screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) + (float) Math.ceil(300 * screenDensity * answerTimer / roundAnswerTimer),
                                    screenHeight - (2 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(10 * screenDensity) - (float) Math.ceil(35 * screenDensity)),
                            (float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);
                }

                if(countdownToStart < 3000)
                {
                    paint.setColor(Color.argb(150, 0, 0, 0));
                    canvas.drawRect(0,0,screenWidth,screenHeight,paint);

                    paint.setTypeface(plain);
                    paint.setTextSize((float) Math.ceil(60 * screenDensity));
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setColor(Color.argb(255, 255, 255, 255));
                    canvas.drawText(gameStatus , screenWidth/2, screenHeight/2, paint);
                }
            }
            else if(isGameOver)
            {
                //Draw message box
                paint.setColor(Color.argb(255,211,229,245));
                canvas.drawRoundRect(new RectF((int)Math.ceil(10 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) ,screenWidth - (int)Math.ceil(10 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(300 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                paint.setColor(Color.argb(255,255,255,255));
                canvas.drawRoundRect(new RectF((int)Math.ceil(14 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(4 * screenDensity) ,screenWidth - (int)Math.ceil(14 * screenDensity), (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(292 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                //Draw gameover Text
                canvas.drawBitmap(gameOverBmp, screenWidth/2 - gameOverBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) - (int)Math.ceil(30 * screenDensity), paint);

                //Draw gameover message
                paint.setTypeface(thin);
                paint.setColor(Color.argb(255, 48, 55, 131));
                paint.setTextSize((float) Math.ceil(25 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("You were not quick to adapt to change", screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(50 * screenDensity), paint);

                //Draw Highscore
                paint.setTypeface(plain);
                paint.setColor(Color.argb(255, 48, 55, 131));
                paint.setTextSize((float) Math.ceil(26 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                if((elapsedTime/1000 + totalBonus) > highscore)
                    canvas.drawText("Highscore : " + (elapsedTime/1000 + totalBonus), screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(100 * screenDensity), paint);
                else
                    canvas.drawText("Highscore : " + highscore, screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(100 * screenDensity), paint);

                //Draw currentscore
                paint.setTextSize((float) Math.ceil(26 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("Current : " + (elapsedTime/1000 + totalBonus), screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(135 * screenDensity), paint);

                canvas.drawBitmap(gameOverWorkerBmp, screenWidth/2 - gameOverWorkerBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(170 * screenDensity), paint);

                //Draw continue button
                canvas.drawBitmap(continueBmp, screenWidth/2 - continueBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(320 * screenDensity), paint);
            }
            else if(isNextStage)
            {
                //Draw message box
                paint.setColor(Color.argb(255,211,229,245));
                canvas.drawRoundRect(new RectF((int)Math.ceil(10 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) ,screenWidth - (int)Math.ceil(10 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(300 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                paint.setColor(Color.argb(255,255,255,255));
                canvas.drawRoundRect(new RectF((int)Math.ceil(14 * screenDensity),(int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(4 * screenDensity) ,screenWidth - (int)Math.ceil(14 * screenDensity), (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(292 * screenDensity)),(float)Math.ceil(5 * screenDensity),(float)Math.ceil(5 * screenDensity),paint);

                //Draw gameover Text
                canvas.drawBitmap(nextLevelBmp, screenWidth/2 - nextLevelBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) - (int)Math.ceil(30 * screenDensity), paint);

                //Draw Highscore
                paint.setTypeface(plain);
                paint.setColor(Color.argb(255, 48, 55, 131));
                paint.setTextSize((float) Math.ceil(26 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("Bonus : " + nextBonus, screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(80 * screenDensity), paint);

                //Draw currentscore
                paint.setTextSize((float) Math.ceil(26 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("Total : " + (elapsedTime/1000 + totalBonus), screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(115 * screenDensity), paint);

                //Draw next stage message
                paint.setTypeface(thin);
                paint.setColor(Color.argb(255, 48, 55, 131));
                paint.setTextSize((float) Math.ceil(25 * screenDensity));
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText(nextStageQuoteLine1[quoteNumber], screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(175 * screenDensity), paint);
                canvas.drawText(nextStageQuoteLine2[quoteNumber], screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(205 * screenDensity), paint);
                canvas.drawText(nextStageQuoteLine3[quoteNumber], screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(235 * screenDensity), paint);
                canvas.drawText(nextStageQuoteLine4[quoteNumber], screenWidth / 2, (int)Math.ceil(gameOverY * screenDensity) + (int) Math.ceil(265 * screenDensity), paint);

                //Draw continue button
                canvas.drawBitmap(continueBmp, screenWidth/2 - continueBmp.getWidth()/2, (int)Math.ceil(gameOverY * screenDensity) + (int)Math.ceil(320 * screenDensity), paint);
            }
//            else if(!isStart)
//            {
//                canvas.drawText("Tap to start", screenWidth / 2, screenHeight / 2, paint);
//            }

            //Draw debug resolution
//            paint.setTextAlign(Paint.Align.LEFT);
//            paint.setTextSize((float) Math.ceil(10 * screenDensity));
//            canvas.drawText(screenHeight + " " + screenWidth, (float) Math.ceil(5 * screenDensity), (float) Math.ceil(10 * screenDensity), paint);

            //Unlocking the canvas
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    public Bitmap makeTintedBitmap(Bitmap src, int color) {
        Bitmap result = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
        Canvas c = new Canvas(result);
        Paint paint = new Paint();
        paint.setColorFilter(new LightingColorFilter(color,0));
        c.drawBitmap(src, 0, 0, paint);
        return result;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int eventAction = event.getAction();

        switch (eventAction) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                if(!isGameOver && isStart && !isNextStage && !isTutorial1Show && !isTutorial2Show && !isTutorial3Show)
                {
                    if(event.getX(0) > screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) &&
                            event.getY(0) > screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity) &&
                            event.getX(0) < screenWidth/2 - 1.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) - (float) Math.ceil(15 * screenDensity) &&
                            event.getY(0) < screenHeight - (1 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity))
                    {
                        Log.d("Button pressed","0");
                        if(gridB[0][0] != 0)
                            calculate(0);
                    }


                    if(event.getX(0) > screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) &&
                            event.getY(0) > screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity) &&
                            event.getX(0) < screenWidth/2 - 0.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) &&
                            event.getY(0) < screenHeight - (1 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity))
                    {
                        Log.d("Button pressed","1");
                        if(gridB[0][0] != 1)
                            calculate(1);
                    }

                    if(event.getX(0) > screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity) &&
                            event.getY(0) > screenHeight - ((1 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity) &&
                            event.getX(0) < screenWidth/2 + 0.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) + (float) Math.ceil(15 * screenDensity) &&
                            event.getY(0) < screenHeight - (1 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) - (float) Math.ceil(15 * screenDensity))
                    {
                        Log.d("Button pressed","2");
                        if(gridB[0][0] != 2)
                            calculate(2);
                    }

                    if(event.getX(0) > screenWidth/2 - (1.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) &&
                            event.getY(0) > screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) &&
                            event.getX(0) < screenWidth/2 - 1.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) - (float) Math.ceil(15 * screenDensity) &&
                            event.getY(0) < screenHeight - (0 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity))
                    {
                        Log.d("Button pressed","3");
                        if(gridB[0][0] != 3)
                            calculate(3);
                    }

                    if(event.getX(0) > screenWidth/2 - (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) &&
                            event.getY(0) > screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) &&
                            event.getX(0) < screenWidth/2 - 0.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) &&
                            event.getY(0) < screenHeight - (0 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity))
                    {
                        Log.d("Button pressed","4");
                        if(gridB[0][0] != 4)
                            calculate(4);
                    }

                    if(event.getX(0) > screenWidth/2 + (0.5f * (float) Math.ceil(90 * screenDensity)) + (0.0f * (float) Math.ceil(90 * screenDensity)) + (float) Math.ceil(15 * screenDensity) &&
                            event.getY(0) > screenHeight - ((0 + 1) * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity) &&
                            event.getX(0) < screenWidth/2 + 0.5f * (float) Math.ceil(90 * screenDensity) + ((0.0f + 1) * (float) Math.ceil((90) * screenDensity)) + (float) Math.ceil(15 * screenDensity) &&
                            event.getY(0) < screenHeight - (0 * (float) Math.ceil(90 * screenDensity)) - (float) Math.ceil(15 * screenDensity))
                    {
                        Log.d("Button pressed","5");
                        if(gridB[0][0] != 5)
                            calculate(5);
                    }
                }
                else if(isGameOver)
                {
//                    init(highscore);
                    returnScore();
                }
                else if(isNextStage)
                {
                    nextStage();
                }
                else if(isTutorial1Show)
                {
                    isTutorial1Show = false;
                    isTutorial2Show = true;

                    tutorialX = (float) Math.ceil(50 * screenDensity);
                    tutorialY = (float) Math.ceil(140 * screenDensity);
                }
                else if(isTutorial2Show)
                {
                    isTutorial2Show = false;
                    isTutorial3Show = true;

                    tutorialX = (float) Math.ceil(330 * screenDensity);
                    tutorialY = (float) Math.ceil(410 * screenDensity);
                }
                else if(isTutorial3Show)
                {
                    isTutorial3Show = false;
                }
//                else if(!isStart)
//                {
//                    isStart = true;
//                }
                break;
        }

        return true;
    }

    public long returnScore()
    {
        return (elapsedTime/1000 + totalBonus);
    }

    private class numberDisplay
    {
        String text;

        float posX;
        float posY;

        public numberDisplay(String text, float posX, float posY)
        {
            this.text = text;

            this.posX = posX;
            this.posY = posY;
        }

        public void moveUp()
        {
            posY -= (float) Math.ceil(2 * screenDensity);
        }
    }

    //game pause
    public void pause() {
        playing = false;
        try {
            gameThread.join();
            soundPool.autoPause();
        } catch (InterruptedException e) {
        }
    }

    //game resume
    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
        soundPool.autoResume();
    }

    public void onDestroy()
    {
        if(bg != null)
        {
            bg.recycle();
            bg = null;
        }
        if(clock != null)
        {
            clock.recycle();
            clock = null;
        }

        if(selectionIcon != null)
        {
            while (selectionIcon.size() < 0)
            {
                selectionIcon.get(0).recycle();
                selectionIcon.remove(0);
            }
            selectionIcon.clear();
        }

        if(gameOverBmp != null)
        {
            gameOverBmp.recycle();
            gameOverBmp = null;
        }
        if(gameOverWorkerBmp != null)
        {
            gameOverWorkerBmp.recycle();
            gameOverWorkerBmp = null;
        }
        if(continueBmp != null)
        {
            continueBmp.recycle();
            continueBmp = null;
        }

        if(nextLevelBmp != null)
        {
            nextLevelBmp.recycle();
            nextLevelBmp = null;
        }
        if(workerBmp != null)
        {
            workerBmp.recycle();
            workerBmp = null;
        }
        if(windowBmp != null)
        {
            windowBmp.recycle();
            windowBmp = null;
        }

        Runtime.getRuntime().gc();
        System.gc();
    }
}